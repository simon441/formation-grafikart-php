<?php

use App\Connection;
use Faker\Factory;
use Symfony\Component\VarDumper\VarDumper;

require dirname(__DIR__) . '/vendor/autoload.php';

const SEPARATOR = PHP_EOL . '-----------------------' . PHP_EOL;

$faker = Factory::create('FR_fr');

$pdo = Connection::getPdo();

/** empty the table */
echo "Empty Database \"tutoblog\"" . SEPARATOR;
$pdo->exec('SET foreign_key_checks = 0;');
$pdo->exec('TRUNCATE TABLE post_category;');
$pdo->exec('TRUNCATE TABLE post;');
$pdo->exec('TRUNCATE TABLE category;');
$pdo->exec('TRUNCATE TABLE user;');
$pdo->exec('SET foreign_key_checks = 1;');

echo PHP_EOL . "Populate TABLE \"Post\"" . SEPARATOR;

$posts = [];
$categories = [];

for ($i = 0; $i < 50; $i++) {
    $query = "INSERT INTO post (post.slug, post.title, post.content, post.created_at)
     VALUES ('{$faker->slug}',
     '{$faker->sentence()}',
     '{$faker->paragraphs(rand(3, 15), true)}',
     '{$faker->date} {$faker->time}');";
    $pdo->exec($query);
    $posts[] = $pdo->lastInsertId();
}

echo PHP_EOL . "Populate TABLE \"category\"" . SEPARATOR;

for ($i = 0; $i < 5; $i++) {
    $query = "INSERT INTO category (category.slug, category.name)
     VALUES ('{$faker->slug}',
     '{$faker->sentence(3)}');";
    $pdo->exec($query);
    $categories[] = $pdo->lastInsertId();
}

echo PHP_EOL . "Populate TABLE \"post_category\"" . SEPARATOR;

foreach ($posts as $post) {
    $randomCategories = $faker->randomElements($categories, rand(0, count($categories)));
    foreach ($randomCategories as $category) {
        $query = "INSERT INTO post_category (post_category.category_id, post_category.post_id) VALUES($category, $post);";
        $pdo->exec($query);
    }
}

echo PHP_EOL . "Populate TABLE \"user\"" . SEPARATOR;

$query = $pdo->prepare("INSERT INTO user (username, password) VALUES (?, ?)");
$query->execute([
    "admin",
    password_hash("admin", PASSWORD_BCRYPT)
]);
echo "done";
