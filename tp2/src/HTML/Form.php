<?php

namespace App\HTML;

class Form
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $errors;

    public function __construct($data, array $errors = [])
    {
        $this->data = $data;
        $this->errors = $errors;
    }

    public function input(string $key, string $label, $type = 'text'): string
    {
        $value = $this->getValue($key);
        return <<<HTML
        <div class="form-group">
            <label for="field{$key}">{$label}</label>
            <input type="{$type}" class="{$this->getInputClass($key)}" id="field{$key}" name="{$key}" value="{$value}" required>
            {$this->getInvalidFeedback($key)}
        </div>
HTML;
    }

    public function textarea(string $key, string $label): string
    {
        $value = $this->getValue($key);
        return <<<HTML
        <div class="form-group">
            <label for="field{$key}">{$label}</label>
            <textarea class="{$this->getInputClass($key)}" id="field{$key}" name="{$key}"  required>{$value}</textarea>   
            {$this->getInvalidFeedback($key)}
        </div>
HTML;
    }

    public function select(string $key, string $label, array $options = []): string
    {
        $optionsHTML = [];

        $value = $this->getValue($key);
        foreach ($options as $k => $v) {
            $selected = in_array($k, $value) ? ' selected ' : '';
            $optionsHTML[] = '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
        }
        $html = implode($optionsHTML);
        return <<<HTML
        <div class="form-group">
            <label for="field{$key}">{$label}</label>
        <select class="{$this->getInputClass($key)}" id="field{$key}" name="{$key}[]" required multiple>{$html}</select>
            {$this->getInvalidFeedback($key)}
        </div>
HTML;
    }

    /**
     * get a value from a key and corvets it for a value to insert into a value=""
     *
     * @param  string $key 
     *
     * @return mixed
     */
    private function getValue(string $key)
    {
        if (is_array($this->data)) {
            return $this->data[$key] ?? null;
        }
        $method = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
        // if (!method_exists($this->data, $method)) {
        //     return null;
        // }
        $value = $this->data->$method();
        if ($value instanceof \DateTimeInterface) {
            return $value->format('Y-m-d H:i:s');
        }
        return $value;
    }

    private function getInputClass(string $key): string
    {
        $inputClass = 'form-control';
        if (isset($this->errors[$key])) {
            $inputClass .= ' is-invalid';
        }
        return $inputClass;
    }

    private function getInvalidFeedback(string $key): string
    {
        if (isset($this->errors[$key])) {
            $error = is_array($this->errors[$key]) ? implode('<br>', $this->errors[$key]) : $this->errors[$key];
            return '<div class="invalid-feedback">' . $error . '</div>';
        }
        return '';
    }
}
