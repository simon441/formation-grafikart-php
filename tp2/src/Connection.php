<?php
namespace App;

use PDO;

class Connection {
    /**
     * @var PDO
     */
    private static $pdo;

    /**
     * @return PDO
     */
    public static function getPdo(): PDO
    {
        if (self::$pdo === null) {
            self::$pdo = new PDO("mysql:dbname=tutoblog;host=127.0.0.1", "root", "root", [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);
            
        }
        return self::$pdo;
    }
}