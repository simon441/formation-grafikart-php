<?php

namespace App;

use AltoRouter;
use App\Security\ForbiddenException;

class Router
{
    /**
     * @var string path to the view directory
     */
    private $viewPath;

    /**
     * @var AltoRouter
     */
    private $router;

    /**
     * @var string Default layout
     */
    private $layout = 'layouts/default';

    private $layoutCondition = null;

    public function __construct(string $viewPath)
    {
        $this->viewPath = $viewPath;
        $this->router = new AltoRouter();
    }

    /**
     * GET method
     *
     * @param  string $url Url to match
     * @param  string $view View name
     * @param  string|null $name Route name
     *
     * @return self
     */
    public function get(string $url, string $view, ?string $name = null): self
    {
        $this->router->map('GET', $url, $view, $name);
        return $this;
    }

    /**
     * POST method
     *
     * @param  string $url Url to match
     * @param  string $view View name
     * @param  string|null $name Route name
     *
     * @return self
     */
    public function post(string $url, string $view, ?string $name = null): self
    {
        $this->router->map('POST', $url, $view, $name);
        return $this;
    }

    /**
     * GET|POST method
     *
     * @param  string $url Url to match
     * @param  string $view View name
     * @param  string|null $name Route name
     *
     * @return self
     */
    public function match(string $url, string $view, ?string $name = null): self
    {
        $this->router->map('GET|POST', $url, $view, $name);
        return $this;
    }

    /**
     * Generates a route
     *
     * @param  string $name Route name
     * @param  array $params Route parameters
     *
     * @return string
     */
    public function url(string $name, array $params = []): string
    {
        return $this->router->generate($name, $params);
    }

    /**
     * Run the server
     *
     * @return self
     */
    public function run(): self
    {
        $match = $this->router->match();
        if ($match !== false) {
            $view = $match['target'];
            $params = $match['params'];
        } else {
            $view = 'errors/e404';
        }
        $router = $this;

        try {
            ob_start();
            require $this->viewPath . DIRECTORY_SEPARATOR . $view . '.php';
            $content = ob_get_clean();
            require $this->viewPath . DIRECTORY_SEPARATOR . $this->chooseLayout($view) . '.php';
        } catch (ForbiddenException $e) {
            header('Location: ' . $this->url('login') . '?forbidden=1');
            exit();
        }
        return $this;
    }

    /**
     * set a layout
     *
     * @param string $layout
     *
     * @return self
     */
    public function setLayout(string $layout): self
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * set a layout
     *
     * @param string $layout le layout à appeler
     * @param string $layoutCondition la préfixe
     *
     * @return self
     */
    public function setLayoutConditional(string $layout, string $layoutCondition = 'admin'): self
    {
        $this->layoutCondition = ['layout' => $layout, 'cond' => $layoutCondition];
        return $this;
    }

    private function chooseLayout($view): string
    {
        $layout = $this->layout;
        if ($this->layoutCondition !== null && strpos($view, $this->layoutCondition['cond']) !== false) {
            $layout = $this->layoutCondition['layout'];
        }
        return $layout;
    }
}
