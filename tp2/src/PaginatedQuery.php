<?php

namespace App;

use PDO;

class PaginatedQuery
{

    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $queryCount;

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $totalPages;

    private $items;

    /**
     * __construct
     * @param string $query
     * @param string $queryCount
     * @param PDO|null $pdo
     * @param int $perPage
     */
    public function __construct(
        string $query,
        string $queryCount,
        ?PDO $pdo = null,
        int $perPage = 12
    ) {
        $this->query = $query;
        $this->queryCount = $queryCount;
        $this->pdo = $pdo ?: Connection::getPdo();
        $this->perPage = $perPage;
    }

    public function getItems(string $classMapping): array
    {
        if ($this->items === null) {
            $currentPage = $this->getCurrentpage();
            $pages = $this->getPages();
            if ($currentPage > $pages) {
                throw new \Exception("Cette page n'existe pas");
            }

            $offset = $this->perPage * ($currentPage - 1);
            $this->items = $this->pdo
                ->query(
                    $this->query .
                        "  LIMIT {$this->perPage} OFFSET $offset"
                )
                ->fetchAll(\PDO::FETCH_CLASS, $classMapping);
        }
        return $this->items;
    }

    /**
     * The HTML for the Previous link
     *
     * @return string|null
     */
    public function previousLink(string $link): ?string
    {
        $currentPage = $this->getCurrentpage();
        if ($currentPage <= 1) return null;
        if ($currentPage > 2) {
            $link .= '?page=' . ($currentPage - 1);
        }
        return <<<HTML
     <a href="{$link}" class="btn btn-primary">&laquo; Page précédente</a>
HTML;
    }
    /**
     * The HTML for the next link
     *
     * @return string|null
     */
    public function nextLink(string $link): ?string
    {
        $currentPage = $this->getCurrentPage();
        $pages = $this->getPages();
        if ($currentPage >= $pages) return null;
        $link .= '?page=' . ($currentPage + 1);
        return <<<HTML
          <a href="{$link}" class="btn btn-primary ml-auto">Page suivante &raquo;</a>
HTML;
    }

    /**
     * getCurrentPage
     *
     * @return int
     */
    private function getCurrentPage(): int
    {
        return Url::getStrictlyPositiveInt('page', 1);
    }

    /**
     * get the total number of pages
     *
     * @return int
     */
    private function getPages(): int
    {
        if ($this->totalPages === null) {
            $this->totalPages = (int) $this->pdo
                ->query($this->queryCount)
                ->fetch(\PDO::FETCH_NUM)[0];
        }

        return ceil($this->totalPages / $this->perPage);
    }
}
