<?php

/**
 * Return a protected string by htmlentities
 *
 * @param  string $string
 *
 * @return string
 */
function e(string $string): string
{
    return htmlentities($string);
}
