<?php

namespace App;

class ObjectHelper
{
    // App\Object::hydrate($post, $_POST, ['title', 'content', 'slug', 'created_at'])
    public static function hydrate(object $model, array $data, array $fields): void
    {
        foreach ($fields as $field) {
            $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
            if (method_exists($model, $method)) {
                $model->$method($data[$field]);
            } elseif (property_exists($model, $field)) {
                $model->$field = $data[$field];
            } else {
                throw new \Exception("{get_class($model)} has no method {$method}");
            }
        }
    }

    public function inflector(string $input): string
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }
}
