<?php

namespace App\Table;

use App\Model\Category;
use App\PaginatedQuery;
use App\Table\Exception\QueryExecutionException;

final class CategoryTable extends Table
{
    protected $table = "category";

    protected $class = Category::class;


    /**
     * Fill an array of Category with Category[]
     *
     * @param  \App\Model\Post[] $posts
     *
     * @return void
     */
    public function hydratePosts(array $posts): void
    {
        $postsById = [];
        foreach ($posts as $post) {
            $post->setCategories([]);
            $postsById[$post->getId()] = $post;
        }
        /**@var Category[] $categories */
        $categories = $this->pdo
            ->query("SELECT c.*, pc.post_id
                FROM post_category pc
                JOIN category c ON c.id = pc.category_id
                WHERE pc.post_id IN (" . implode(",", array_keys($postsById)) . ")")
            ->fetchAll(\PDO::FETCH_CLASS, Category::class);

        // On parcourt les catégories
        foreach ($categories as $category) {
            $postsById[$category->getPostId()]->addCategory($category);
        }
    }

    /**
     * @return array [Category[], PaginatedQuery]
     */
    public function findPaginated(): array
    {
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM {$this->table} ORDER BY name DESC ",
            "SELECT COUNT(id) FROM {$this->table}",
            $this->pdo
        );

        /** @var post[] $categories */
        $categories = $paginatedQuery->getItems(Category::class);
        return [$categories, $paginatedQuery];
    }

    public function all(): array
    {
        return $this->queryAndFetchAll("SELECT * FROM {$this->table} ORDER BY id DESC");
    }

    /**
     * Get a list of id => name
     *
     * @return array
     */
    public function list(): array
    {
        $categories =  $this->queryAndFetchAll("SELECT * FROM {$this->table} ORDER BY name ASC");
        $results = [];
        foreach ($categories as $category) {
            $results[$category->getId()] = $category->getName();
        }
        return $results;
    }
}
