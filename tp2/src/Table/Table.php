<?php

namespace App\Table;

use App\PaginatedQuery;
use App\Table\Exception\NotFoundException;
use App\Table\Exception\QueryExecutionException;
use PDO;

abstract class Table
{
    /**
     * @var PDO $pdo
     */
    protected $pdo;

    protected $table = null;

    protected $class = null;

    public function __construct(PDO $pdo)
    {
        if ($this->table === null) {
            throw new \Exception("La classe " . get_class($this) . " doir avoir une table définie");
        }
        if ($this->class === null) {
            throw new \Exception("La classe " . get_class($this) . " doit avoir une class définie");
        }
        $this->pdo = $pdo;
    }

    /**
     * find an id in a table
     *
     * @param  int $id Id to find in table
     * @throws NotFoundException
     * @return object
     */
    public function find(int $id)
    {
        $query = $this->pdo->prepare('SELECT * FROM ' . $this->table . ' WHERE id = :id');
        $query->execute(['id' => $id]);
        $query->setFetchMode(PDO::FETCH_CLASS, $this->class);
        $result = $query->fetch();
        if ($result === false) {
            throw new NotFoundException($this->table, $id);
        }
        return $result;
    }

    /**
     * Verifies if a value exists in the table on a field
     *
     * @param  string $field Field to search
     * @param  mixed $value Value associated to the field
     * @param  int|null $value Exception?
     *
     * @return bool
     */
    public function exists(string $field, $value, ?int $except = null): bool
    {
        $sql = "SELECT COUNT(id) FROM {$this->table} WHERE $field = ?";
        $params = [$value];
        if ($except !== null) {
            $sql .= " AND id != ?";
            $params[] = $except;
        }
        $query = $this->pdo->prepare($sql);
        $query->execute($params);
        return (int) $query->fetch(\PDO::FETCH_NUM)[0] > 0;
    }

    public function all(): array
    {
        $sql = "SELECT * FROM {$this->table}";
        return $query = $this->pdo->query($sql, PDO::FETCH_CLASS, $this->class)->fetchAll();
    }

    // /**
    //  * @param string|null $fieldOrderBy
    //  * @return array [object[], PaginatedQuery]
    //  */
    // public function findPaginated(?string $fieldOrderBy = null): array
    // {
    //     $paginatedQuery = new PaginatedQuery(
    //         "SELECT * FROM {$this->table} ORDER BY {$fieldOrderBy} DESC",
    //         "SELECT COUNT(id) FROM {$this->table}",
    //         $this->pdo
    //     );

    //     $items = $paginatedQuery->getItems($this->class);
    //     return [$items, $paginatedQuery];
    // }

    /**
     * delete a record
     *
     * @param  int $id
     * @throws QueryExecutionException
     *
     * @return bool
     */
    public function delete(int $id): void
    {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE id = ?");
        $ok = $query->execute([$id]);
        if ($ok === false) {
            throw new QueryExecutionException($this->table, $id, 'supprimer');
        }
    }

    /**
     * create
     *
     * @param  array $data
     * @throws QueryExecutionException
     *
     * @return int
     */
    public function create(array $data): int
    {
        $sqlParts = [];
        foreach ($data as $key => $value) {
            $sqlParts[] = ":$key";
        }
        $query = $this->pdo->prepare("INSERT INTO {$this->table} (" . implode(', ', array_keys($data)) . ") VALUES (" . implode(", ", $sqlParts) . "); ");
        $ok = $query->execute($data);
        $id = $this->pdo->lastInsertId();
        // $post->setId($id);
        if ($ok === false) {
            throw new QueryExecutionException($this->table, null, 'créer');
        }
        return (int) $id;
    }

    /**
     * update
     *
     * @param  array $data
     * @throws QueryExecutionException
     * 
     * @return void
     */
    public function update(array $data, int $id)
    {
        $sqlParts = [];
        foreach ($data as $key => $value) {
            $sqlParts[] = "$key = :$key";
        }
        $query = $this->pdo->prepare("UPDATE {$this->table} SET " . implode(', ', $sqlParts) . " WHERE id = :id");
        $ok = $query->execute(array_merge($data, ['id' => $id]));
        if ($ok === false) {
            throw new QueryExecutionException($this->table, $id, 'mettre à jour');
        }
    }

    /**
     *
     * @param string $sql
     *
     * @return object[]
     */
    public function queryAndFetchAll(string $sql): array
    {
        return $this->pdo->query($sql, PDO::FETCH_CLASS, $this->class)->fetchAll();
    }

    /**
     *
     * @param string $sql
     * @param array $params
     *
     * @return object
     */
    public function prepAndFetch(string $sql, array $params = []): object
    {
        $query = $this->pdo->prepare($sql);
        $query->execute($params);
        $query->setFetchMode(PDO::FETCH_CLASS, $this->class);
        $result = $query->fetch();
        if ($result === false) {
            throw new NotFoundException($this->table, "'" . implode("', '", $params) . "'");
        }
        return $result;
    }

    /**
     *
     * @param string $sql
     * @param array $params
     *
     * @return object[]
     */
    public function prepAndFetchAll(string $sql, array $params = []): array
    {
        $query = $this->pdo->prepare($sql);
        $ok = $query->execute($params);
        return  $query->fetchAll(\PDO::FETCH_CLASS, $this->class);
    }
}
