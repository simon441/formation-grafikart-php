<?php

namespace App\Table\Exception;

class QueryExecutionException extends \Exception
{
    public function __construct(string $table, ?int $id = null, string $action = "faire l'action sur ")
    {
        if ($id === null) {
            $this->message = "Impossible de {$action} l'enregistrement avec l'id #$id dans la table '$table'";
        } else {
            $this->message = "Impossible de {$action} l'enregistrement dans la table '$table'";
        }
    }
}
