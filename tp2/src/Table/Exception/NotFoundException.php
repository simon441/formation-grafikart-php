<?php

namespace App\Table\Exception;

class NotFoundException extends \Exception
{
    public function __construct(string $table, $id)
    {
        // if (is_int($id)) {
        $this->message = "Aucun enregistrement ne correspond à l'id #$id dans la table '$table'";
        // } else {
        //     $this->message = "Aucun enregistrement ne correspond au champ '{$id}' demandé dans la table '$table'";
        // }
    }
}
