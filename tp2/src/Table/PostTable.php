<?php

namespace App\Table;

use App\Model\Category;
use App\Model\Post;
use App\PaginatedQuery;
use App\Table\Exception\QueryExecutionException;

final class PostTable extends Table
{

    protected $table = "post";

    protected $class = Post::class;

    /**
     * create
     *
     * @param  Post $post
     * @throws QueryExecutionException
     *
     * @return int
     */
    public function createPost(Post $post): int
    {
        $id = $this->create([
            'title' => $post->getTitle(),
            'slug' => $post->getSlug(),
            'content' => $post->getContent(),
            'created_at' => $post->getCreatedAt()->format('Y-m-d H:i:s')
        ]);;
        $post->setId($id);
        return $id;
    }

    /**
     * update
     *
     * @param  Post $post
     * @param  array $categories
     * @throws QueryExecutionException
     * 
     * @return void
     */
    public function updatePost(Post $post): void
    {
        $this->update([
            'title' => $post->getTitle(),
            'slug' => $post->getSlug(),
            'content' => $post->getContent(),
            'created_at' => $post->getCreatedAt()->format('Y-m-d H:i:s')
        ], $post->getId());
    }

    public function attachCategories(int $id, array $categories): void
    {
        $this->pdo->exec("DELETE FROM post_category WHERE post_id = " . $id);
        $query = $this->pdo->prepare("INSERT INTO post_category (post_id, category_id) VALUES (:pid, :cid);");
        foreach ($categories as $category) {
            $query->execute(['pid' => $id, 'cid' => $category]);
        }
    }


    /**
     * @return array [Post[], PaginatedQuery]
     */
    public function findPaginated(): array
    {
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM {$this->table} ORDER BY created_at DESC ",
            "SELECT COUNT(id) FROM {$this->table}",
            $this->pdo
        );

        /** @var Post[] $posts */
        $posts = $paginatedQuery->getItems(Post::class);
        (new CategoryTable($this->pdo))->hydratePosts($posts);
        return [$posts, $paginatedQuery];
    }

    /**
     * @param int $id Id of the category to find
     * @return array [Category[], PaginatedQuery]
     */
    public function findPaginatedForCategory(int $categoryId): array
    {
        $paginatedQuery = new PaginatedQuery(
            "SELECT p.* 
           FROM {$this->table}  p
           JOIN post_category pc ON pc.post_id = p.id
           WHERE pc.category_id = {$categoryId}
           ORDER BY created_at DESC",
            "SELECT COUNT(post_id) 
           FROM post_category 
           WHERE category_id = {$categoryId}"
        );

        /**@var Post[] $posts */
        $posts = $paginatedQuery->getItems(Post::class);
        (new CategoryTable($this->pdo))->hydratePosts($posts);
        return [$posts, $paginatedQuery];
    }
}
