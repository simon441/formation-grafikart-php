<?php
namespace App;

class Url {
    /**
     * get an integer 
     *
     * @param  mixed $name
     * @param  mixed $default
     *
     * @return int
     */
    public static function getInt(string $name, ?int $default = null) : ?int
    {
        if (!isset($_GET[$name])) return $default;
        if ($_GET[$name] === '0') return 0;
        if (!filter_var($_GET[$name], FILTER_VALIDATE_INT))  {
            throw new \Exception("Le paramètre '{$name}' dans l'url n'est pas un entier"); 
        }
        return (int)$_GET[$name];
    }
    
    /**
     * get an integer > 0
     *
     * @param  mixed $name
     * @param  mixed $default
     *
     * @return int
     */
    public static function getStrictlyPositiveInt(string $name, ?int $default = null) : ?int
    {
        $param = self::getInt($name, $default);
        if ($param !== null && $param <= 0) {
            throw new \Exception("Le paramètre '{$name}' dans l'url n'est pas un entier supérieur ou égal à un"); 
        }
        return $param;
    }
        
    /**
     * GEt an integer >= 0
     *
     * @param  mixed $name
     * @param  mixed $default
     *
     * @return int
     */
    public static function getPositiveInt(string $name, ?int $default = null) : ?int
    {
        $param = self::getInt($name, $default);
        if ($param !== null && $param < 0) {
            throw new \Exception("Le paramètre '{$name}' dans l'url n'est pas un entier positif"); 
        }
        return $param;
    }
        
}