<?php

namespace App\Validators;

use App\Table\PostTable;

class PostValidator extends AbstractValidator
{

    public function __construct(array $data, PostTable $postTable, ?int $postId = null, array $categories = [])
    {
        parent::__construct($data);
        $this->validator->rule('required', ['title', 'slug', 'categories_ids']);
        $this->validator->rule('lengthBetween',  ['title', 'slug'], 3, 200);
        $this->validator->rule('lengthMin',  ['content'], 3);
        $this->validator->rule('slug', 'slug');
        $this->validator->rule('subset', 'categories_id', array_keys($categories));
        $this->validator->rule(function ($field, $value) use ($postTable, $postId) {
            return !$postTable->exists($field, $value, $postId);
        }, ['slug', 'title'], 'Cette valeur est déjà utilisée');
        $this->validator->rule('subset', 'categories_id', array_keys($categories));
        $this->validator->rule('dateFormat', 'created_at', 'Y-m-d H:i:s');
        $this->validator->rule('containsUnique', 'categories_id', array_keys($categories));
        // $v->labels([
        //     'title' => 'Le titre',
        //     'content' => 'Le contenu',
        // ]);
    }
}
