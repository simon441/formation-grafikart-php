<?php

namespace App\Model;

class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $name;

    private $post_id;

    /**
     * @var Post
     */
    private $post;

    /**
     * getId
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * getTitle
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * getSlug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;;
        return $this;
    }

    /**
     * getPostId
     *
     * @return string
     */
    public function getPostId(): ?int
    {
        return $this->post_id;
    }

    /**
     * setPost
     *
     * @return void
     */
    public function setPost(Post $post): void
    {
        $this->post = $post;
    }
}
