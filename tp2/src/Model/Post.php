<?php

namespace App\Model;

use App\Helpers\Text;
use DateTime;

class Post
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $created_at;

    /**
     * @var Category[]
     */
    private $categories = [];

    /**
     * getId
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * getTitle
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * getSlug
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }


    /**
     * getContent
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * getCreatedAt
     *
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return new DateTime($this->created_at);
    }

    /**
     * setId
     *
     * @param  int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * setTitle
     *
     * @param string
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * setSlug
     * 
     * @param string
     * @return self
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;;

        return $this;
    }

    /**
     * setContent
     *
     * @param string
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * setCreatedAt
     * 
     * @param string
     * @return self
     */
    public function setCreatedAt($createdAt): self
    {
        $this->created_at = $createdAt;
        return $this;
    }

    /**
     * getFormattedContent
     *
     * @return string
     */
    public function getFormattedContent(): string
    {
        return nl2br(htmlentities($this->content));
    }

    /**
     * getCategories
     *
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * getCategories
     *
     * @return Category[]
     */
    public function getCategoriesIds(): array
    {
        return array_map(function ($category) {
            return $category->getId();
        }, $this->categories);
        // $ids = [];
        // foreach ($this->categories  as $category) {
        //     $ids[] = $category->getId();
        // }
        // return $ids;
    }

    /**
     * 
     *
     * @param  Category[] $categories
     *
     * @return self
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * addCategory
     *
     * @param  Category $category
     *
     * @return void
     */
    public function addCategory(Category $category): void
    {
        $category->setPost($this);
        $this->categories[] = $category;
        $category->setPost($this);
    }



    /**
     * Get an excerpt from the content of the Post
     *
     * @return string Except content
     */
    public function getExcerpt(): ?string
    {
        if ($this->content === null) {
            return null;
        }
        return nl2br(htmlentities(Text::excerpt($this->content, 60)));
    }
}
