<?php

use App\Router;

require '../vendor/autoload.php';

define('DEBUG_TIME', microtime(true));

// define('VIEW_PATH', dirname(__DIR__) . '/views');

/* $router = new AltoRouter();

$router->map('GET', '/blog', function () {
    require VIEW_PATH . '/post/index.php';
});

$router->map('GET', '/blog/category', function () {
    require VIEW_PATH . '/category/show.php';
});
$match = $router->match();
$match['target']();
 */

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

if (isset($_GET['page']) && $_GET['page'] === '1') {
    $uri = explode('?', $_SERVER['REQUEST_URI'])[0];
    $get = $_GET;
    unset($get['page']);
    $query = http_build_query($get);
    $uri = $uri . (empty($query) ? '' : '?' . $query);
    // if (!empty($query)) {
    //     $uri = $uri. '?' . $query;
    // }
    header('Location: ' . $uri);
    http_response_code(301);
    exit();
}

$router = new Router(dirname(__DIR__) . '/views');

$router
    ->get('/', 'post/index', 'home')
    // $router->get('/blog/category', 'category/show', 'category');
    ->get('/blog/category/[*:slug]-[i:id]', 'category/show', 'category')
    ->get('/blog/[*:slug]-[i:id]', 'post/show', 'post')

    // login
    ->match('/login', 'auth/login', 'login')
    ->post('/logout', 'auth/logout', 'logout')

    // AMIN
    // gestion des articles
    ->get('/admin', 'admin/post/index', 'admin_posts')
    ->match('/admin/post/[i:id]', 'admin/post/edit', 'admin_post_edit')
    ->post('/admin/post/[i:id]/delete', 'admin/post/delete', 'admin_post_delete')
    ->match('/admin/post/new', 'admin/post/new', 'admin_post_new')

    // gestion des catégories
    ->get('/admin/category', 'admin/category/index', 'admin_categories')
    ->match('/admin/category/[i:id]', 'admin/category/edit', 'admin_category_edit')
    ->post('/admin/category/[i:id]/delete', 'admin/category/delete', 'admin_category_delete')
    ->setLayoutConditional('admin/layouts/default', 'admin')

    ->match('/contact', 'pages/contact', 'contact')
    ->get('/error/notfound', 'admin/category/new', 'error404')

    ->run();
