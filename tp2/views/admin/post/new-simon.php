<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Post;
use App\ObjectHelper;
use App\Table\PostTable;
use App\Validator;
use App\Validators\PostValidator;

$title = "Créer un article";
$pdo = Connection::getPdo();
/** @var Post $post */
$post = new Post();
$success = false;

$errors = [];

if (!empty($_POST)) {
    Validator::lang('fr');

    $postTable = new PostTable($pdo);
    $v = new PostValidator($_POST, $postTable);


    // $v->labels([
    //     'title' => 'Le titre',
    //     'content' => 'Le contenu',
    // ]);
    // if (empty($_POST['title'])) {
    //     $errors['title'][] = "Le champ titre ne peux pas être vide";
    // }
    // if (mb_strlen($_POST['title']) <= 3) {
    //     $errors['title'][] = "Le champ titre doit contenir plus de trois caractères";
    // }

    // $post
    //     ->setTitle($_POST['title'])
    //     ->setSlug($_POST['slug'])
    //     ->setContent($_POST['content'])
    //     ->setCreatedAt($_POST['created_at']);

    ObjectHelper::hydrate($post, $_POST, ['title', 'slug', 'content', 'created_at']);

    if ($v->validate()) {
        $postTable->create($post);
        $success = true;
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($post, $errors);
?>

<?php if ($success) : ?>
    <div class="alert alert-success">
        L'article a bien été créé
    </div>
<?php endif; ?>

<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        L'article n'a pas pu être créé, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Créer un nouvel article</h1>

<form action="" method="post">
    <?= $form->input('title', 'Titre'); ?>
    <?= $form->input('slug', 'URL'); ?>
    <?= $form->textarea('content', 'Contenu'); ?>
    <?= $form->input('created_at', 'Date de création'); ?>
    <?php /*<div class="form-group">
        <label for="title">Titre</label>
        <input type="text" class="form-control <?= isset($errors['title']) ? 'is-invalid' : '' ?>" id="title" name="title" value="" required>
        <?php if (array_key_exists('title', $errors)) : ?>
            <div class="invalid-feedback">
                <?= implode('<br>', $errors['title']); ?>
            </div>
        <?php endif; ?>
    </div>*/ ?>
    <button class="btn btn-primary">Modifier</button>
</form>

<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des articles</a>