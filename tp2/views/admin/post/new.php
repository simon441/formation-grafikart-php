<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Post;
use App\ObjectHelper;
use App\Table\CategoryTable;
use App\Table\PostTable;
use App\Validator;
use App\Validators\PostValidator;

require dirname(__DIR__) . '/variables.php';

$title = "Créer un article";
$pdo = Connection::getPdo();
/** @var Post $item */
$item = new Post();
$item->setCreatedAt(date('Y-m-d H:i:s'));

$categoryTable = new CategoryTable($pdo);
$categories = $categoryTable->list();

$errors = [];

if (!empty($_POST)) {

    $table = new PostTable($pdo);
    $v = new PostValidator($_POST, $table, null, $categories);

    ObjectHelper::hydrate($item, $_POST, ['title', 'slug', 'content', 'created_at']);

    if ($v->validate()) {
        $pdo->beginTransaction();
        $table->createPost($item);
        $table->attachCategories($item->getId(), $_POST['categories_ids']);
        $pdo->commit();
        $success = true;
        // header('Location: ' . $router->url('admin_post_edit', ['id' => $item->getId()]) . '?created=1');
        header('Location: ' . $router->url('admin_posts') . '?created=1');
        exit();
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($item, $errors);
?>
<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        L'article n'a pas pu être enregistré, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Créer un article</h1>

<?php require '_form.php'; ?>
<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des articles</a>