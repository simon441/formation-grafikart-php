<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Post;
use App\ObjectHelper;
use App\Table\CategoryTable;
use App\Table\PostTable;
use App\Validator;
use App\Validators\PostValidator;

require dirname(__DIR__) . '/variables.php';

$title = "Editer l'article";
$pdo = Connection::getPdo();
$table = new PostTable($pdo);
$categoryTable = new CategoryTable($pdo);
$categories = $categoryTable->list();

/** @var Post $item */
$item = $table->find($params['id']);
$categoryTable->hydratePosts([$item]);

$success = false;

$errors = [];

if (!empty($_POST)) {

    $v = new PostValidator($_POST, $table, $item->getId());


    // $v->labels([
    //     'title' => 'Le titre',
    //     'content' => 'Le contenu',
    // ]);
    // if (empty($_POST['title'])) {
    //     $errors['title'][] = "Le champ titre ne peux pas être vide";
    // }
    // if (mb_strlen($_POST['title']) <= 3) {
    //     $errors['title'][] = "Le champ titre doit contenir plus de trois caractères";
    // }

    // $item
    //     ->setTitle($_POST['title'])
    //     ->setSlug($_POST['slug'])
    //     ->setContent($_POST['content'])
    //     ->setCreatedAt($_POST['created_at']);

    ObjectHelper::hydrate($item, $_POST, ['title', 'slug', 'content', 'created_at']);

    if ($v->validate()) {
        $pdo->beginTransaction();
        $table->updatePost($item);
        $table->attachCategories($item->getId(), $_POST['categories_ids']);
        $pdo->commit();
        $categoryTable->hydratePosts([$item]);
        $success = true;
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($item, $errors);
?>
<?php if (isset($_GET['created'])) : ?>
    <div class="alert alert-success">
        L'article a bien été enregistré
    </div>
<?php endif; ?>
<?php if ($success) : ?>
    <div class="alert alert-success">
        L'article a bien été modifié
    </div>
<?php endif; ?>

<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        L'article n'a pas pu être modifié, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Editer l'article <?= e($item->getTitle()); ?></h1>

<?php require '_form.php'; ?>

<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des articles</a>