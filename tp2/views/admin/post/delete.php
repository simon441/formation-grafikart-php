<?php

use App\Auth;
use App\Connection;
use App\Table\PostTable;

Auth::check();

$title = "Mon blog";
$pdo = Connection::getPdo();

$id = (int) $params['id'];

$table = new PostTable($pdo);
$table->delete($id);

header('Location: ' . $router->url('admin_posts') . '?delete=1');
