<form action="" method="post">
    <?= $form->input('title', 'Titre'); ?>
    <?= $form->input('slug', 'URL'); ?>
    <?= $form->select('categories_ids', 'Catégories', $categories); ?>
    <?= $form->textarea('content', 'Contenu'); ?>
    <?= $form->input('created_at', 'Date de création'); ?>
    <button class="btn btn-primary"><?= $item->getId() === null ? 'Créer' : 'Modifier'; ?></button>
</form>