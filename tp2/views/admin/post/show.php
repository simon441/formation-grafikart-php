<?php

use App\Connection;
use App\Model\Post;
use App\Table\CategoryTable;
use App\Table\PostTable;

$title = '';
$id = (int) $params['id'];
$slug = $params['slug'];

$pdo = Connection::getPdo();

/** @var Post|false $post */
$post = (new PostTable($pdo))->find($id);
(new CategoryTable($pdo))->hydratePosts([$post]);

if ($post->getSlug() !== $slug) {
    $url = $router->url('post', ['slug' => $post->getSlug(), 'id' => $id]);
    header('Location: ' . $url);
    http_response_code(301);
    exit();
}

$title = e($post->getTitle());

?>
<h1><?= e($post->getTitle()); ?></h1>
<p class="text-muted"><?= $post->getCreatedAt()->format('d F Y H:i'); ?></p>
<?php foreach ($post->getCategories() as $k => $category) :
    $link = $router->url('category', ['id' => $category->getId(), 'slug' => $category->getSlug()]);
?><?php if ($k > 0) : ?>, <?php endif; ?><a href="<?= $link; ?>"><?= $category->getName(); ?></a><?php endforeach; ?>
<p><?= $post->getFormattedContent(); ?></p>