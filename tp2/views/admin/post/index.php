<?php

use App\Auth;
use App\Connection;
use App\Table\PostTable;

Auth::check();

$title = "Administration";
$pdo = Connection::getPdo();

// variables
$perPage = 12;

$table = new PostTable($pdo);
$list = $table->findPaginated();
/**PHP < 7.1 */
// $posts = $list[0];
// $pagination = $list[1];
/** PHP >= 7.1 */
/** @var Post[] $posts */
[$posts, $pagination] = $table->findPaginated();

$link = $router->url('admin_posts');
$cardType = "l'article";
?>

<?php if (isset($_GET['delete'])) : ?>
    <div class="alert alert-success">
        L'enregistrement a bien été supprimé
    </div>
<?php endif; ?>
<?php if (isset($_GET['created'])) : ?>
    <div class="alert alert-success">
        L'article a bien été enregistré
    </div>
<?php endif; ?>

<table class="table">
    <tr>
        <th>#</th>
        <th>Titre</th>
        <th>
            <a href="<?= $router->url('admin_post_new'); ?>" class="btn btn-outline-primary">Nouvel Article</a>
        </th>
    </tr>
    <?php foreach ($posts as $post) : ?>
        <tr>
            <td>#<?= $post->getId(); ?></td>
            <td>
                <a href="<?= $router->url('admin_post_edit', ['id' => $post->getId()]); ?>">
                    <?= e($post->getTitle()); ?>
                </a>
            </td>
            <td>
                <a href="<?= $router->url('admin_post_edit', ['id' => $post->getId()]); ?>" class="btn btn-primary">Editer</a>
                <form action="<?= $router->url('admin_post_delete', ['id' =>  $post->getId()]); ?>" method="post" onsubmit="return confirm('Voulez-vous vraiment effectuer cette action?');" style="display: inline;">
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<div class="d-flex justify-content-between my-4">

    <?= $pagination->previousLink($link); ?>
    <?= $pagination->nextLink($link); ?>
</div>