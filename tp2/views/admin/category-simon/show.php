<?php

use App\Connection;
use App\Model\Category;
use App\Model\Post;
use App\Table\CategoryTable;
use App\Table\PostTable;

$title = '';
$id = (int) $params['id'];
$slug = $params['slug'];

$pdo = Connection::getPdo();

/** @var Category|false $category */
$category = (new CategoryTable($pdo))->find($id);

$title = e($category->getName());

?>
<h1><?= e($category->getName()); ?></h1>