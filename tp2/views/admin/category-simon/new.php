<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Category;
use App\Model\Post;
use App\ObjectHelper;
use App\Table\CategoryTable;
use App\Validators\CategoryValidator;

require dirname(__DIR__) . '/variables.php';

$title = "Créer une catégorie";
$pdo = Connection::getPdo();
/** @var Category $category */
$category = new Category();

$errors = [];

if (!empty($_POST)) {

    $categoryTable = new CategoryTable($pdo);
    $v = new CategoryValidator($_POST, $categoryTable);

    ObjectHelper::hydrate($category, $_POST, ['name', 'slug']);

    if ($v->validate()) {
        $categoryTable->create($category);
        $success = true;
        // header('Location: ' . $router->url('admin_post_edit', ['id' => $category->getId()]) . '?created=1');
        header('Location: ' . $router->url('admin_categories'));
        exit();
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($category, $errors);
?>
<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        La catégorie n'a pas pu être enregistré, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Créer une catégorie</h1>

<?php require '_form.php'; ?>
<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des catégories</a>