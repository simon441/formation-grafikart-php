<?php

use App\Auth;
use App\Connection;
use App\Table\CategoryTable;

Auth::check();

$title = "Administration";
$pdo = Connection::getPdo();

$table = new CategoryTable($pdo);
[$categories, $pagination] = $table->findPaginated();

$link = $router->url('admin_categories');
$cardType = "la catégorie";
?>

<?php if (isset($_GET['delete'])) : ?>
    <div class="alert alert-success">
        L'enregistrement a bien été supprimé
    </div>
<?php endif; ?>
<?php if (isset($_GET['created'])) : ?>
    <div class="alert alert-success">
        Le catégorie a bien été enregistré
    </div>
<?php endif; ?>

<table class="table">
    <tr>
        <th>#</th>
        <th>Nom</th>
        <th>
            <a href="<?= $router->url('admin_category_new'); ?>" class="btn btn-outline-primary">Nouvelle Catégorie</a>
        </th>
    </tr>
    <?php foreach ($categories as $category) : ?>
        <tr>
            <td>#<?= $category->getId(); ?></td>
            <td>
                <a href="<?= $router->url('admin_category_edit', ['id' => $category->getId()]); ?>">
                    <?= e($category->getName()); ?>
                </a>
            </td>
            <td>
                <a href="<?= $router->url('admin_category_edit', ['id' => $category->getId()]); ?>" class="btn btn-primary">Editer</a>
                <form action="<?= $router->url('admin_category_delete', ['id' =>  $category->getId()]); ?>" method="post" onsubmit="return confirm('Voulez-vous vraiment effectuer cette action?');" style="display: inline;">
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<div class="d-flex justify-content-between my-4">

    <?= $pagination->previousLink($link); ?>
    <?= $pagination->nextLink($link); ?>
</div>