<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Category;
use App\ObjectHelper;
use App\Table\CategoryTable;
use App\Validators\CategoryValidator;

require dirname(__DIR__) . '/variables.php';

$title = "Editer l'article";
$pdo = Connection::getPdo();
$table = new CategoryTable($pdo);
/** @var Category $category */
$category = $table->find($params['id']);

$success = false;

$errors = [];

if (!empty($_POST)) {

    $v = new CategoryValidator($_POST, $table, $category->getId());

    ObjectHelper::hydrate($category, $_POST, ['name', 'slug']);

    if ($v->validate()) {
        $table->update($category);
        $success = true;
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($category, $errors);
?>
<?php if (isset($_GET['created'])) : ?>
    <div class="alert alert-success">
        La catégorie a bien été enregistrée
    </div>
<?php endif; ?>
<?php if ($success) : ?>
    <div class="alert alert-success">
        La catégorie a bien été modifiée
    </div>
<?php endif; ?>

<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        La catégorie n'a pas pu être modifiée, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Editer l'article <?= e($category->getName()); ?></h1>

<?php require '_form.php'; ?>

<a href="<?= $router->url('admin_categories') ?>">Retour à la liste des catégories</a>