<?php

use App\Auth;
use App\Connection;
use App\HTML\Form;
use App\Model\Category;
use App\ObjectHelper;
use App\Table\CategoryTable;
use App\Validators\CategoryValidator;

require dirname(__DIR__) . '/variables.php';

Auth::check();

$title = "Créer une catégorie";
$pdo = Connection::getPdo();
/** @var Category $item */
$item = new Category();

$errors = [];

if (!empty($_POST)) {

    $table = new CategoryTable($pdo);
    $v = new CategoryValidator($_POST, $table);

    ObjectHelper::hydrate($item, $_POST, ['name', 'slug']);

    if ($v->validate()) {
        $table->create([
            'name' => $item->getName(),
            'slug' => $item->getSlug()
        ]);
        $success = true;
        // header('Location: ' . $router->url('admin_post_edit', ['id' => $item->getId()]) . '?created=1');
        header('Location: ' . $router->url('admin_categories') . '?created=1');
        exit();
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($item, $errors);
?>
<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        La catégorie n'a pas pu être enregistrée, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Créer une catégorie</h1>

<?php require '_form.php'; ?>
<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des catégories</a>