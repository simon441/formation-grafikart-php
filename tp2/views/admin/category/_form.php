<form action="" method="post">
    <?= $form->input('name', 'Nom'); ?>
    <?= $form->input('slug', 'URL'); ?>
    <button class="btn btn-primary"><?= $item->getId() === null ? 'Créer' : 'Modifier'; ?></button>
</form>