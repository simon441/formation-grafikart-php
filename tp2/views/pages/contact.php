<?php
$title = 'Nous contacter';
?>
<div class="row">
    <div class="ml-auto mr-auto col-md-8">
        <h1>Nous contacter</h1>
        <form action="" method="post" class="contact-form">
            <div class="form-group">
                <label for="yourname">Votre nom</label>
                <input class="form-control" type="text" name="name" id="yourname">
            </div>
            <div class="form-group">
                <label for="youremail">Votre email</label>
                <input class="form-control" type="email" name="email" id="youremail">
            </div>
            <div class="form-group">

                <label for="messagetitle">Titre de votre message</label>
                <input class="form-control" type="text" name="title" id="messagetitle">
            </div>
            <div class="form-group">
                <label for="message">Votre message</label>
                <textarea class="form-control" id="message" name="message"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Envoyer</button>
        </form>
    </div>
</div>