<?php
session_start();
unset($_SESSION['user']);
session_destroy();

header('Location: ' . $router->url('login') . '?logout=1');
exit();
