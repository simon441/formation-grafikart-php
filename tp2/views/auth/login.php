<?php

use App\Connection;
use App\HTML\Form;
use App\Model\User;
use App\Table\Exception\NotFoundException;
use App\Table\UserTable;

$errors = [];
$errorMsg = "Identifiants et mot de passe incorrects";

$user = new User();

if (!empty($_POST)) {
    $errors["username"] =  $errors["password"] = $errorMsg;
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $table = new UserTable(Connection::getPdo());
        try {
            $u = $table->findByUsername($_POST['username']);
            if (password_verify($_POST['password'], $u->getPassword()) === true) {
                $errors["username"] =  $errors["password"] = '';
                session_start();
                $_SESSION['auth'] = $u->getId();
                header('Location: ' . $router->url('admin_posts'));
                exit();
            }
        } catch (NotFoundException $e) {
        }
    }
}

$form = new Form($user, $errors);

$title = 'Se connecter';
?>

<?php if (isset($_GET['forbidden'])) : ?>
    <div class="alert alert-danger">
        Vous ne pouvez pas accéder à cette page
    </div>
<?php endif; ?>
<?php if (isset($_GET['logout'])) : ?>
    <div class="alert alert-success">
        Vous vous êtes bien déconnecté
    </div>
<?php endif; ?>
<div class="container-fluid">
    <h1>Se connecter</h1>
    <form action="<?= $router->url('login'); ?>" method="post">
        <?= $form->input('username', "Nom d'utilisateur"); ?>
        <?= $form->input('password', "Mot de passe", 'password'); ?>
        <button class="btn btn-outline-primary">Se connecter</button>
    </form>
</div>