<?php
$title = 'Page introuvable';
http_response_code(404);
?>
<h1>Désolé, mais la page demandée n'a pu être trouvée.</h1>
<p>Vous pouvez nous contacter par la <a href="<?= $router->url('contact'); ?>">page de contact</a></p>