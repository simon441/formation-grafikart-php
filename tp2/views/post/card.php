<?php
// On crée un tableau contenant la syntaxe HTML d'un lien
// [
//     '<a href="/category/123">Categorie</a>',
//     '<a href="/category/123">Categorie</a>',
//     '<a href="/category/123">Categorie</a>',
//     '<a href="/category/123">Categorie</a>',
// ]
// On regroupe les éléments du tableau avec une virgule
/** MOI
$links = [];
foreach ($post->getCategories() as $category) {
    $link = $router->url('category', ['id' => $category->getId(), 'slug' => $category->getSlug()]);
    $name = e($category->getName());
    $links[] = <<<HTML
    <a href="{$link}">{$name}</a>
HTML;
}
dd($links);
*/
/** @var \App\Post $post */
/** grafikart*/
/* $categories = [];
foreach ($post->getCategories() as $category) {
    $link = $router->url('category', ['id' => $category->getId(), 'slug' => $category->getSlug()]);
    $name = e($category->getName());
    $categories[] = <<<HTML
    <a href="{$link}">{$name}</a>
HTML;
} */
$categories = array_map(function($category) use($router) {
    $link = $router->url('category', ['id' => $category->getId(), 'slug' => $category->getSlug()]);
    $name = e($category->getName());
    return <<<HTML
    <a href="{$link}">{$name}</a>
HTML;
}, $post->getCategories());
?>
<div class="card mb-3">
    <h5 class="card-title"><?= e($post->getTitle()); ?></h5>
    <p class="text-muted">
        <?= $post->getCreatedAt()->format('d F Y H:i'); ?>
        <?php if (!empty($post->getCategories())): ?> &nbsp;::&nbsp;<?=implode(', ', $categories);?><?php endif;?>
    </p>
    <p><?= $post->getExcerpt(); ?></p>
    <p>
        <a href="<?= $router->url('post', ['id' => $post->getId(), 'slug' => $post->getSlug()]); ?>" class="btn btn-outline-primary">Voir plus <span class="sr-only">de <?= $cardType . ' '. $post->getTitle(); ?></span></a>
    </p>
</div>