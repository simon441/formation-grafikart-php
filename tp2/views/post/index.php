<?php

use App\Connection;
use App\Table\PostTable;

$title = "Mon blog";
$pdo = Connection::getPdo();

// variables
$perPage = 12;

$table = new PostTable($pdo);
$list = $table->findPaginated();
/**PHP < 7.1 */
// $posts = $list[0];
// $pagination = $list[1];
/** PHP >= 7.1 */
[$posts, $pagination] = $table->findPaginated();

$link = $router->url('home');
$cardType = "l'article";
?>
<h1>Mon blog</h1>

<div class="row">
    <?php foreach ($posts as $post) : ?>
        <div class="col-md-3">
            <?php require 'card.php'; ?>
        </div>
    <?php endforeach; ?>
</div>

<div class="d-flex justify-content-between my-4">

    <?= $pagination->previousLink($link); ?>
    <?= $pagination->nextLink($link); ?>
</div>