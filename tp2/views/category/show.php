<?php

use App\Connection;
use App\Model\Category;
use App\Router;
use App\Table\CategoryTable;
use App\Table\PostTable;

$title = '';
$id = (int) $params['id'];
$slug = $params['slug'];

$pdo = Connection::getPdo();

/**@var Category $category */
$category = (new CategoryTable($pdo))->find($id);

if ($category->getSlug() !== $slug) {
    $url = $router->url('category', ['slug' => $category->getSlug(), 'id' => $id]);
    header('Location: ' . $url);
    http_response_code(301);
    exit();
}

[$posts, $pagination] = (new PostTable($pdo))->findPaginatedForCategory($category->getId());

$title = "Catégorie " . e($category->getName());

/** @var Router $router */
$link = $router->url('category', ['id' => $category->getId(), 'slug' => $category->getSlug()]);
$cardType = "la catégorie";
?>
<h1><?= $title; ?></h1>

<div class="row">
    <?php foreach ($posts as $post) : ?>
        <div class="col-md-3">
            <?php require dirname(__DIR__) . '/post/card.php'; ?>
        </div>
    <?php endforeach; ?>
</div>

<div class="d-flex justify-content-between my-4">
    <?= $pagination->previousLink($link); ?>
    <?= $pagination->nextLink($link); ?>

</div>