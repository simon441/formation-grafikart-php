<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Post;
use App\ObjectHelper;
use App\Table\PostTable;
use App\Validator;
use App\Validators\PostValidator;

require dirname(__DIR__) . '/variables.php';

$title = "Créer un article";
$pdo = Connection::getPdo();
/** @var Post $post */
$post = new Post();
$post->setCreatedAt(date('Y-m-d H:i:s'));

$errors = [];

if (!empty($_POST)) {

    $postTable = new PostTable($pdo);
    $v = new PostValidator($_POST, $postTable);

    ObjectHelper::hydrate($post, $_POST, ['title', 'slug', 'content', 'created_at']);

    if ($v->validate()) {
        $postTable->create([$post]);
        $success = true;
        // header('Location: ' . $router->url('admin_post_edit', ['id' => $post->getId()]) . '?created=1');
        header('Location: ' . $router->url('admin_posts'));
        exit();
    } else {
        $errors = $v->errors();
    }
}

$form = new Form($post, $errors);
?>
<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger">
        L'article n'a pas pu être enregistré, merci de corriger vos erreurs
    </div>
<?php endif; ?>
<h1>Créer un article</h1>

<?php require '_form.php'; ?>
<a href="<?= $router->url('admin_posts') ?>">Retour à la liste des articles</a>