<?php
require_once 'elements/functions.php';

$file = __DIR__ . DIRECTORY_SEPARATOR . 'emails' . DIRECTORY_SEPARATOR . date('Y-m-d'); // '2020-20-12'; 

$title = 'Newsletter';

$success = null;
$error = null;

$classAlert = '';
$message = '';

$email = ($_POST['email']) ?? null;
if (!empty($email)) {
    $emailFiltered = filter_var($email, FILTER_VALIDATE_EMAIL);
    dump($email, $emailFiltered, $file);
    if ($emailFiltered) {
        file_put_contents($file, $email . "\n", FILE_APPEND);
        $success = true;
        $classAlert = 'success';
    } else {
        $error = true;
        $classAlert = 'danger';
    }
}


require 'elements/header.php';
?>
<h1>Newsletter</h1>

<div class="row">
    <?php if ($success) : ?>
        <div class="alert alert-success">
            Vous avez été inscrit à la liste de diffusion.
        </div>
    <?php elseif ($error) : ?>
        <div class="alert alert-danger">
            Désolé, une erreur est survenue. Veuillez réessayer.
        </div>
    <?php endif; ?>

    <form action="" method="POST">
        <!--      <div class="col-md-5">-->
        <div class="form-group">
            <label for="email">Votre email
                <input type="email" name="email" id="email" placeholder="Votre email..." required class="">
            </label>
            <button type="submit" class="btn btn-primary">S'abonner</button>
            <!--   </div>-->
        </div>
    </form>

</div>

<?php require 'elements/footer.php'; ?>