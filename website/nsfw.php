<?php

/** not safe for work 
 * ===================
 * Système de limitation aux -18 ans ne peuvent accéder à la page
 * 
 */
$cookieName = 'birthday';
$age = null;

if (!empty($_POST['birthday'])) {
    setcookie($cookieName, $_POST['birthday']);
    $birthday = $_POST['birthday'];
    $_COOKIE[$cookieName] = $_POST['birthday'];
}

if (!empty($_COOKIE[$cookieName])) {
    $birthday = (int) $_COOKIE[$cookieName];
    $age = (int) date('Y') - $birthday;
}

$list = array_combine(range(2012, 1919), range(2012, 1919));

require 'elements/header.php';

?>
<!--

- Demander à l'utilisateur sa date de naissance (champ select de 2012 à 1919)
- Persiter la date de naissance dans un cookie (cookie de session)
- Si l'utilisateur est assez agé, lui montrer le contenu
- Sinon on affiche un message d'erreur
-->

<?php if ($age >= 18) : ?>
    <h1>Du contenu réservé aux adultes</h1>
<?php elseif ($age !== null) : ?>
    <div class="alert alert-danger">Vous n'avez pas l'âge requis pour voir le contenu</div>
<?php else : ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="birthday">Section réservée pour les adultes, enter votre année de naissance : </label>
            <select name="birthday" id="birthday" class="form-control">
                <?php for ($i = 2010; $i > 1919; $i--) : ?>
                    <option value="<?= $i; ?>"><?= $i; ?></option>
                <?php endfor; ?>
            </select>

        </div>
        <button class="btn btn-primary">Envoyer</button>
    </form>
<?php endif; ?>
<?php $removeNewsletterFooter = true;
require 'elements/footer.php'; ?>