<?php

namespace App\Helpers;

class Form
{

    const INPUT_TYPES = [
        'text',
        'number',
        'email',
        'search',
        'date',
        'password',
        'hidden',

    ];
    /**
     * Class to be used on the checkboxes
     * @var string
     */
    public static $class = 'form-control';


    /**
     * radio
     * creates a radio field
     *
     * @param  string $name The field name
     * @param  string $value The field Value
     * @param  string $name The type of field (text|email|number)
     * @param  array $attributes The input attributes
     *
     * @return string
     */
    public static function input(string $name, string $value = null, string $type = 'text', $attributes = []): string
    {
        // $attributes = '';
        $class = self::class;

        if (!in_array($type, self::INPUT_TYPES)) {
            $type = 'text';
        }
        if (in_array('class', $attributes)) {
            $attributes['class']  .= ' ' . self::class;
        } else {
            $attributes['class'] = self::class;
        }
        $attributes = implode(' ', $attributes);

        return <<<HTML
    <input type="text" name="{$name}" class="{self:}" value="$value" $attributes>
HTML;
    }


    /**
     * checkbox
     * creates a checkbox field
     *
     * @param  string $name The field name
     * @param  string|null $value The field VAlue
     * @param  array $data The input data
     *
     * @return string
     */
    public static function checkbox(string $name, ?string $value = null, array $data = []): string
    {
        $attributes = '';
        if (isset($data[$name]) && in_array($value, $data[$name])) {
            $attributes .= ' checked';
        }
        $attributes = ' class="' . self::$class . '"';
        return <<<HTML
    <input type="checkbox" name="{$name}[]" value="$value" $attributes>
HTML;
    }


    /**
     * radio
     * creates a radio field
     *
     * @param  string $name The field name
     * @param  string $value The field Value
     * @param  array $data The input data
     *
     * @return string
     */
    public static function radio(string $name, string $value = null, array $data = []): string
    {
        $attributes = '';
        if (isset($data[$name]) && $value === $data[$name]) {
            $attributes .= ' checked';
        }
        return <<<HTML
    <input type="radio" name="{$name}" value="$value" $attributes>
HTML;
    }

    /**
     * select
     * creates a select field
     *
     * @param  string $name The field name
     * @param  string $value The field value
     * @param  string $options The field <option>
     * @param  array|string $attributes The attributes to add to the field (class="...", etc...) as array or string
     *
     * @return string
     */
    public static function select(string $name, string $value, array $options, $attributes = ''): string
    {
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
        $htmlOptions = [];
        foreach ($options as $key => $option) {
            $attributesOptions = $key === $value ? 'selected' : '';
            $htmlOptions[] = "<option value='$key' $attributesOptions>$option</option>";
        }
        return  "<select name='$name' " . implode(' ', $attributes) . ">" . implode($htmlOptions) . "</select>";
    }
}
