<?php

namespace App\Helpers;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Counter.php';


class DoubleCounter extends Counter
{
    const INCREMENT = 10;

    /**
     * @see {inheritdoc}
     *
     * @return int
     */
    public function get(): int
    {
        return 2 * parent::get();
    }
}
