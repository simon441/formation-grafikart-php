<?php

namespace App\Blog;

use Parsedown;

class Post
{

    public static $maxExcerpt = 150;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $content;

    /**
     * @var DateTime|string|int
     */
    public $created_at;

    public function __construct()
    {
        if (is_int($this->created_at) || is_string($this->created_at)) {
            $this->created_at =  new \DateTime('@' . $this->created_at);
        }
    }

    /**
     * Return an except of the content field
     *
     * @return string
     */
    public function getExcerpt()
    {
        return substr($this->content, 0, self::$maxExcerpt);
    }

    /**
     * getBody
     *
     * @return string
     */
    public function getBody(bool $excerpt = false): string
    {
        $parsedown = new Parsedown();
        $parsedown->setSafeMode(true);
        $body = $parsedown->text($this->content);
        return $excerpt ? substr($body, 0, self::$maxExcerpt) : $body;
    }
}
