<?php

namespace App\Exceptions;

/**
 * Manages an Exception from the curl extension's Connection
 */
class CurlException extends \Exception
{
    /**
     * __construct
     *
     * @param  ressource $curl Curl connection
     *
     * @return void
     */
    public function __construct($curl)
    {
        $this->message = curl_error($curl);
        curl_close($curl);
    }
}
