<?php

namespace App;

use DateTime;
use App\Exceptions\CurlException;
use App\Exceptions\HTTPException;
use App\Exceptions\UnauthorizedHTTPException;

/**
 * Manage the Openweather API
 *
 * @author Sorani Riku <sorani.dev@outlook.com>
 *
 * @since 1.0
 *
 */
class OpenWeather
{
    /**
     * @var string current and valid API key to connect to the OpenWeatherMap API
     */
    private $apiKey;

    /**
     * @var string file to dump the result of the curl command to
     */
    public $file = '';

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }


    /**
     * Retrive today's weather information
     * @var string $city 'City' or 'City, CountryCode'
     * @var int|null $id city id
     * 
     * @return null|array[]
     */
    public function getToday(?string $city = null, ?int $id = null): ?array
    {
        // try {
        $data = $this->getForecastData('weather', $city, $id);
        // } catch (APIException $e) {
        //     return [
        //         'temp' => 0,
        //         'description' => 'Météo indisponible',
        //         'date' => new DateTime(),
        //     ];
        // }

        $data = json_decode($data, true);

        return [
            'temp' => (float) $data['main']['temp'],
            'description' => $data['weather'][0]['description'],
            'date' => new DateTime('@' . $data['dt']),
        ];
    }

    /**
     * Weather on the 5 nex days for a city
     * @var string $city 'City' or 'City, CountryCode' @example 'Paris, FR'
     * @var int|null $id city id @example 10012001
     * 
     * @return null|array[]
     */
    public function getForecast(?string $city = null, ?int $id = null): ?array
    {

        $data = $this->getForecastData('forecast', $city, $id);

        $data = json_decode($data, true);

        $results = [];

        $currentDate = null;
        $i = 0;

        foreach ($data['list'] as $day) {
            $date =  new DateTime('@' . $day['dt']);
            // echo '<hr>';
            //// var_dump($i, $currentDate, $date);
            // if ($currentDate === null) {
            //     $currentDate = $date;
            // } elseif ($currentDate->d < $date->d) {
            //     $currentDate = $date;

            //     $i++;
            // }
            $results[] = [
                'temp' => (float) $day['main']['temp'],
                'description' => $day['weather'][0]['description'],
                'date' => $date,
            ];
            // $results[$i][] = [
            //     'temp' => (float) $day['main']['temp'],
            //     'description' => $day['weather'][0]['description'],
            //     'date' => $date,
            // ];
        }

        return $results;
    }
    /**
     * Retreive the weather for the 5 nex days for a city as as JSON Response
     * @var string $city 'City' or 'City, CountryCode' @example 'Paris, FR'
     * @var int|null $id city id @example 10012001
     * 
     * @return void
     */
    public function getForecastAsJson(?string $city = null, ?int $id = null): void
    {
        $code = 200;
        header('Content-Type: application/json');
        $data = $this->getForecastData('forecast', $city, $id);
        if ($data === null) {
            $code = 400;
        }
        http_response_code($code);
        echo $data; //json_encode($data);
        exit();
    }

    /**
     * Get forecast data from the Api
     * @var string $type type of request to the API (weather|forecast)
     * @var string $city 'City' or 'City, CountryCode' @example 'Paris, FR'
     * @var int|null $id city id @example 10012001
     * 
     * @return null|array[]
     */
    private function getForecastData(string $type, ?string $city = null, ?int $id = null): ?string
    {

        if ($city === null && $id !== 0) {
            $question = sprintf('id=%d', $id);
        } else {
            $question = 'q=' . $city;
        }

        // try {
        $data = $this->callApi("$type?{$question}");
        // } catch (APIException $e) {
        //     die($e->getMessage());
        // return [];
        // }
        return $data;
    }

    /**
     * Call the Open weather API via CURL and saves the results in a cached file to avoid calling the API every time
     *
     * @param  string $endpoint what is calling for the API (weather|forecast) withe the corresponsding arguments
     * @throws CurlException if the curl gets an error
     * @throws HTTPException if the status code recieved is different than 200 (ok HTTP_OK)
     * @throws UnauthorizedHTTPException if the response is 301 (unauthorized by the remote server)
     * 
     * @return string
     */
    private function callApi(string $endpoint): ?string
    {
        $statusCode = 200;
        $curl = null;
        $url = "";
        $unit = 'metric';
        $lang = 'fr';

        $file = $this->file . md5($endpoint) . '.json';
        $url = "http://api.openweathermap.org/data/2.5/{$endpoint}&APPID={$this->apiKey}&units={$unit}&lang={$lang}";

        if (file_exists($file)) {
            $data = file_get_contents($file);
            // header('Content-TYpe: application/json');
            // echo $data;
            // die;
            //// var_dump($data);
            // $data = json_decode($data, true);
            // return [[
            //     'temp' => (float) $data['main']['temp'],
            //     'description' => $data['weather'][0]['description'],
            //     'date' => new DateTime(),
            // ]];
            // $data = [[
            //     'temp' => 5.03,
            //     'description' => 'ciel dégagé',
            //     'date' => new DateTime(),
            // ]];
            // die;
        } else {
            $curl = curl_init($url);
            curl_setopt_array($curl, [
                CURLOPT_CAINFO => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'openweathermap-org.pem',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 1
            ]);
            $data = curl_exec($curl);
            //// var_dump($data);


            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($data === false) {
                throw new CurlException($curl);
            }

            file_put_contents($file, $data);
        }
        //var_dump(__METHOD__, $data, $statusCode);
        if ($statusCode !== 200) {
            if ($statusCode === 401) {
                $data = json_decode($data, true);
                throw new UnauthorizedHTTPException($data['message'], 401);
            }
            throw new UnauthorizedHTTPException($data, $statusCode);
        }

        // header('Content-TYpe:application/json');
        // echo $data;
        // die;

        if ($curl) {
            curl_close($curl);
        }

        return $data;
    }
}
