<?php

namespace App\Guestbook;

use DateTime;
use DateTimeZone;

class Message
{
    const LIMIT_USERNAME = 3;
    const LIMIT_MESSAGE = 10;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $message;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var string[]
     */
    private $errors;


    /**
     * __construct
     *
     * @param  string $username
     * @param  string $message
     * @param  DateTime $date
     *
     * @return void
     */
    public function __construct(string $username, string $message, ?DateTime $date = null)
    {
        $this->username = $username;
        $this->message = $message;
        $this->date = $date ?: new DateTime();
    }

    /**
     * isValid
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return empty($this->getErrors());
    }

    /**
     * getErrors
     *
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];
        if (strlen($this->username) < self::LIMIT_USERNAME) {
            $errors['username'] = "Votre pseudo est trop court";
        }
        if (strlen($this->message) < self::LIMIT_MESSAGE) {
            $errors['message'] = "Votre message est trop court";
        }
        return $errors;
    }

    /**
     * toHTML
     *
     * @return string
     */
    public function toHTML(): string
    {
        $pseudo = htmlentities($this->username);
        $this->date->setTimezone(new DateTimeZone('Europe/Paris'));
        $date = $this->date->format('d/m/Y à H:i');
        $message = nl2br(htmlentities($this->message));
        return <<<HTML
        <p>
            <strong>$pseudo</strong> <em>le $date</em><br>
            $message
        </p>
HTML;
    }

    /**
     * toJSON
     *
     * @return string
     */
    public function toJSON(): string
    {
        // $message = new Message($this->username, $this->message, $this->date);
        return json_encode([
            'username' => $this->username,
            'message' => $this->message,
            'date' => $this->date->getTimestamp(),
        ]);
    }

    /**
     * fromJSON
     *
     * @param  string $json
     *
     * @return Message
     */
    public static function fromJSON(string $json): Message
    {
        $data = json_decode($json, true);
        return new self($data['username'], $data['message'], new DateTime('@' . $data['date']));
    }
}
