<?php

namespace App\Guestbook;

// require_once 'Message.php';

class GuestBook
{
    /**
     * @var string
     */
    private $file;

    /**
     * __construct
     *
     * @param  string $file
     *
     * @return void
     */
    public function __construct(string $file)
    {
        $directory = dirname($file);
        // create dir if not exists
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        // create file if not exists
        if (!file_exists($file)) {
            touch($file);
        }
        $this->file = $file;
    }

    /**
     * addMessage
     *
     * @param  Message $message
     *
     * @return void
     */
    public function addMessage(Message $message): void
    {
        file_put_contents($this->file, $message->toJSON() . PHP_EOL, FILE_APPEND);
    }

    /**
     * getMessages
     *
     * @return Message[]
     */
    public function getMessages(): array
    {

        $content = trim(file_get_contents($this->file));
        $lines = explode(PHP_EOL, $content);

        $messages = [];
        foreach ($lines as $line) {
            $messages[] = Message::fromJSON($line);
        }
        return array_reverse($messages);
    }
}
