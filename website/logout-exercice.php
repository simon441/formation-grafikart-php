<?php
session_start();

unset($_SESSION['userid']);
unset($_SESSION['username']);

session_regenerate_id();
header('location:login.php');
exit();
