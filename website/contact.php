<?php
$title = 'Nous contacter';
require_once 'vendor/autoload.php';

require_once 'elements/config.php';
// require_once 'elements/functions.php';


/** chercher si le magasin est ouvert maintenant */
date_default_timezone_set('Europe/Paris');
// Récupérer l'heure d'aujourd'hui $heure
$heure = (int) ($_GET['hour'] ??  date('G'));
$day = (int) ($_GET['day'] ??  date('N') - 1);
// Récupérer les creaneaux d'aujourd'hui $creneaux
$creneaux = CRENEAUX[$day];
// Récupérer l'état d'ouverture du magasin
// $ouvert = inCreaneaux($heure, [[0, 19]]);
$ouvert = inCreaneaux($heure, $creneaux);
// $color = $ouvert ? 'green' : 'red';
$classAlert = $ouvert ? 'success' : 'danger';
$textOpened = $ouvert ? 'ouvert' : 'fermé';

$verb = (isset($_GET['hour']) && isset($_GET['day'])) ? 'sera' : 'est';

require 'elements/header.php';
?>
<div class="row">
    <div class="col-md 8">
        <h2>Nous contacter</h2>

        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tenetur explicabo odio facere optio amet? Modi ullam reiciendis incidunt aspernatur aliquid cum consectetur optio vitae, fuga nobis iure iste numquam facilis!</p>
    </div>
    <div class="col-md-4">
        <h2>Horaires d'ouverture</h2>
        <?php /*<div class="alert alert-<?= $classAlert ?>">
            Le magasin sera <?= $textOpened; ?> le <?= JOURS[$day] ?> à <?= $heure; ?>h
        </div>*/ ?>

        <?php if ($ouvert) : ?>
            <div class="alert alert-success">
                Le magasin <?= $verb; ?> ouvert
            </div>
        <?php else : ?>
            <div class="alert alert-danger">
                Le magasin <?= $verb; ?> fermé
            </div>
        <?php endif; ?>
        <form action="" method="GET">
            <div class="form-group">
                <!--<label for="selectDay">Choisir le jour </label>-->

                <?= select('day', $day, JOURS, 'class="form-control"'); ?>
            </div>
            <div class="form-group">
                <!--<label for="hour">Choisir l'heure </label>-->
                <input type="number" name="hour" id="hour" value="<?= $heure; ?>" placeholder="heure"> heures
            </div>
            <button type="submit" class="btn btn-primary">Voir si le magasin est ouvert</button>
        </form>

        <!-- De 9h à 12h et de 14h à 19h -->
        <?php /* $creneaux */ ?>
        <ul>
            <?php foreach (JOURS as $key => $jour) : ?>
                <li <?php if ($key + 1 === (int) date('N')) : ?> <?php endif; ?>> <strong><?= $jour; ?></strong> : <?= creaneauxHtml(CRENEAUX[$key]); ?></li>
            <?php endforeach; ?>
        </ul>

    </div>
</div>
<!-- https://www.grafikart.fr/tutoriels/dates-php-1124 : 20:01 -->
<!-- https://youtu.be/HgIlzi6QzSc?t=1201 -->
<?php require 'elements/footer.php'; ?>