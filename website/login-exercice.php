<?php
session_start();
require_once 'elements/functions.php';

$title = 'Newsletter';

$success = null;
$error = null;
$username = null;
$password = null;

if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // if ($) {
    if ($username === 'user' && $password === 'demo') {
        $_SESSION['username'] = $username;
        $_SESSION['userid'] = $username;
        var_dump($_SESSION);
        $success = true;
    } else {
        $error = "Les dentifiants sont invalides";
    }

    if ($success) {
        header('location: dashboard.php');
        exit();
    }
}

require 'elements/header.php';
?>
<h1>Se connecter</h1>

<?php if ($error) : ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php endif; ?>
<div class="">
    <form action="" method="POST">
        <div class="form-group">
            <input type="text" name="username" placeholder="Entrer votre nom d'utilisateur" required class="form-control" value="" autocomplete="off">
        </div>
        <div class="form-group">
            <input type="password" name="password" placeholder="Entrer votre mot de passe" required class="form-control" value="" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>

</div>

<?php $removeNewsletterFooter = true;
require 'elements/footer.php'; ?>