<?php
require_once '../vendor/autoload.php';
// require '../elements/functions.php';


try {
    $pdo = new PDO('sqlite:../data/blog.db', null, null, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]);
} catch (\PDOException $e) {
    var_dump($e->getTraceAsString());
    die();
}

$error = null;
$success = null;

try {
    if (isset($_POST['delete'])) {
        $query = $pdo->prepare('DELETE FROM posts WHERE id=:id');
        $query->execute([
            'id' => $_GET['id'],
        ]);
        $success = 'Votre article a bien été supprimé';
    }
    $query = $pdo->prepare('SELECT * FROM posts WHERE id=:id');
    $query->execute(['id' => $_GET['id']]);
    // if ($query === false) {
    //     var_dump($pdo->errorInfo());
    //     die('Error SQL');
    // }

    $post = $query->fetch();
} catch (\PDOException $e) {
    $error = $e->getMessage();
}

$pdo = null;

require '../elements/header.php';
?>
<div class="container">
    <p>
        <a href="/blog">Revenir au listing </a>
        <a href="/blog/show.php?id=<?= $post->id; ?>">Revenir au post </a>
    </p>
    <h1>Delete</h1>
    <?php if ($error) : ?>
        <div class="alter alert-danger"><?= $error; ?></div>
    <?php elseif ($success) : ?>
        <div class="alert alert-success">
            <?= $success; ?>
        </div>
    <?php else : ?>
        <form action="" method="post">
            <p>Voulez vous vraiment effacer le post suivant<br>
                <blockquote><?= htmlentities($post->title); ?><br><?= htmlentities($post->content); ?></blockquote>
            </p>
            <input type="hidden" name="delete">
            <!-- <button type="reset" class="btn btn-danger">Annuler</button> -->
            <button type="submit" class="btn btn-primary">Supprimer</button>
        </form>
    <?php endif; ?>
</div>

<?php
$counterEnabled = false;
$removeNewsletterFooter = true;
require '../elements/footer.php';
