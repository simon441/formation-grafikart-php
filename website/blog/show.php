<?php
require_once '../vendor/autoload.php';

use App\Blog\Post;

// require_once '../elements/functions.php';
// require '../class/Post.php';


try {
    $pdo = new PDO('sqlite:../data/blog.db', null, null, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]);
} catch (\PDOException $e) {
    var_dump($e->getTraceAsString());
    die();
}

$error = null;
$success = null;

try {
    if (isset($_POST['title']) && isset($_POST['content'])) {
        $query = $pdo->prepare('UPDATE posts SET title=:title, content=:content WHERE id=:id');
        $query->execute([
            'title' => $_POST['title'],
            'content' => $_POST['content'],
            'id' => $_GET['id'],
        ]);
        $success = 'Votre article a bien été modifié';
    }
    $query = $pdo->prepare('SELECT * FROM posts WHERE id=:id');
    $query->execute(['id' => $_GET['id']]);
    // if ($query === false) {
    //     var_dump($pdo->errorInfo());
    //     die('Error SQL');
    // }

    $post = $query->fetchObject(Post::class);
} catch (\PDOException $e) {
    $error = $e->getMessage();
}

$pdo = null;

require '../elements/header.php';
?>
<div class="container">
    <p>
        <a href="/blog">Revenir au listing </a>
    </p>
    <h1><?= htmlentities($post->title); ?></h1>
    <p class="small text-muted">Ecrit le <?= $post->created_at->format('d/m/Y H:i:s'); ?></p>
    <p>
        <?php //= nl2br(htmlentities($post->content)); 
        ?>
        <?= $post->getBody(); ?>
    </p>
    <p>
        <a href="/blog/edit.php?id=<?= $post->id; ?>" class="col-md-6">Modifier</a><a href="/blog/delete.php?id=<?= $post->id; ?>">Supprimer</a>
    </p>
</div> <?php
        $counterEnabled = false;
        $removeNewsletterFooter = true;
        require '../elements/footer.php';
