<?php
require_once '../vendor/autoload.php';

use App\Blog\Post;

// require '../elements/functions.php';
// require '../class/Post.php';


try {
    $pdo = new PDO('sqlite:../data/blog.db', null, null, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]);
} catch (\PDOException $e) {
    var_dump($e->getTraceAsString());
    die();
}

$error = null;
try {
    if (isset($_POST['title']) && isset($_POST['content'])) {
        $query = $pdo->prepare('INSERT INTO posts (title, content, created_at) VALUES(:title, :content, :created_at)');
        $query->execute([
            'title' => $_POST['title'],
            'content' => $_POST['content'],
            'created_at' => time(),
        ]);
        header('Location: /blog/show.php?id=' . $pdo->lastInsertId());
    }
    $query = $pdo->query('SELECT * FROM posts ORDER BY created_at DESC');
    // if ($query === false) {
    //     var_dump($pdo->errorInfo());
    //     die('Error SQL');
    // }

    /** @var Post[] */
    $posts = $query->fetchAll(PDO::FETCH_CLASS, Post::class);
} catch (\PDOException $e) {
    $error = $e->getMessage();
}

$pdo = null;

require '../elements/header.php';
?>
<div class="container">
    <?php if ($error) : ?>
        <div class="alter alert-danger"><?= $error; ?></div>
    <?php else : ?>
        <ul>
            <?php foreach ($posts as $post) : ?>
                <h2><a href="/blog/show.php?id=<?= $post->id; ?>"><?= htmlentities($post->title); ?></a></h2>
                <p class="small text-muted">Ecrit le <?= $post->created_at->format('d/m/Y H:i:s'); ?></p>
                <!-- <a href="/blog/delete.php?id=<?= $post->id; ?>" class="btn btn-danger">Supprimer</a> -->
                <p>
                    <?php //= nl2br(htmlentities($post->getExcerpt())); 
                    ?>
                    <?= $post->getBody(true); ?>
                </p>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <form action="" method="post">
        <div class="form-group">
            <label for="name" class="sr-only">Entrer le titre: </label>
            <input type="text" id="ntitleme" class="form-control" name="title" placeholder="Entrer le titre">
        </div>
        <div class="form-group">
            <label for="usersurname" class="sr-only">Entrer le contenu: </label>
            <textarea id="usersurname" class="form-control" name="content" placeholder="Entrer le contenu"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Créer</button>
    </form>
</div>

<?php
$counterEnabled = false;
$removeNewsletterFooter = true;
require '../elements/footer.php';
