<?php
require '../elements/functions.php';


try {
    $pdo = new PDO('sqlite:../data/blog.db', null, null, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]);
} catch (\PDOException $e) {
    var_dump($e->getTraceAsString());
    die();
}

$pdo->beginTransaction();
$pdo->exec('UPDATE posts SET title = "demo" WHERE id = 4');
$pdo->exec('UPDATE posts SET content = "demo" WHERE id = 4');
$post = $pdo->query('SELECT * FROM posts  WHERE id = 4')->fetch();
var_dump($post);
$pdo->rollBack();

// $post = $pdo->query('SELECT * FROM posts  WHERE id = 4')->fetch();
// var_dump($post);
$pdo = null;
