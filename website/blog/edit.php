<?php
// require '../elements/functions.php';

use App\Blog\Post;

require_once '../vendor/autoload.php';

try {
    $pdo = new PDO('sqlite:../data/blog.db', null, null, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]);
} catch (\PDOException $e) {
    var_dump($e->getTraceAsString());
    die();
}

$error = null;
$success = null;

try {
    if (isset($_POST['title']) && isset($_POST['content'])) {
        $query = $pdo->prepare('UPDATE posts SET title=:title, content=:content WHERE id=:id');
        $query->execute([
            'title' => $_POST['title'],
            'content' => $_POST['content'],
            'id' => $_GET['id'],
        ]);
        $success = 'Votre article a bien été modifié';
    }
    $query = $pdo->prepare('SELECT * FROM posts WHERE id=:id');
    $query->execute(['id' => $_GET['id']]);
    // if ($query === false) {
    //     var_dump($pdo->errorInfo());
    //     die('Error SQL');
    // }

    $post = $query->fetchObject(Post::class);
} catch (\PDOException $e) {
    $error = $e->getMessage();
}

$pdo = null;

require '../elements/header.php';
?>
<div class="container">
    <p>
        <a href="/blog">Revenir au listing </a>
        <a href="/blog/show.php?id=<?= $post->id; ?>">Revenir au post </a>
    </p>
    <h1>Edition</h1>
    <?php if ($error) : ?>
        <div class="alter alert-danger"><?= $error; ?></div>
    <?php elseif ($success) : ?>
        <div class="alert alert-success">
            <?= $success; ?>
        </div>
    <?php endif; ?>
    <form action="" method="post">
        <div class="form-group">
            <label for="name" class="sr-only">Entrer le titre: </label>
            <input type="text" id="ntitleme" class="form-control" value="<?= htmlentities($post->title); ?>" name="title" placeholder="Entrer le titre">
        </div>
        <div class="form-group">
            <label for="content" class="sr-only">Entrer le contenu: </label>
            <textarea id="content" class="form-control" name="content" placeholder="Entrer le contenu"><?= nl2br(($post->content)) ?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
    </form>
</div>

<?php
$counterEnabled = false;
$removeNewsletterFooter = true;
require '../elements/footer.php';
