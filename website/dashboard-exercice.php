<?php
session_start();
require 'functions/session.php';

if (!isset($_SESSION['userid']) || !verifySession($_SESSION['userid'])) {
    header('Location: login.php');
    exit();
}
var_dump($_SESSION);
require_once 'functions/counter.php';

$year = (int) date("Y");
$selectedYear = empty($_GET['year']) ? null : (int) $_GET['year'];
$selectedMonth = empty($_GET['month']) ? null : $_GET['month'];
if ($selectedYear && $selectedMonth) {
    $total = numberViewedPerMonth($selectedYear, $selectedMonth);
    $details = numberViewedPerMonthDetailed($selectedYear, $selectedMonth);
} else {
    $total = numberViewed();
}


$months = array(
    '01' => 'Janvier',
    '02' => 'Février',
    '03' => 'Mars',
    '04' => 'Avril',
    '05' => 'Mai',
    '06' => 'Juin',
    '07' => 'Juillet',
    '08' => 'Août',
    '09' => 'Septembre',
    '10' => 'Octobre',
    '11' => 'Novembre',
    '12' => 'Décembre'
);

require 'elements/header.php';
?>
<div class="row">
    <div class="col-md-4">
        <div class="list-group">
            <?php for ($i = 0; $i < 5; $i++) : ?>
                <a class="list-group-item <?= $year - $i === $selectedYear ? 'active' : ''; ?>" href="dashboard.php?year=<?= $year - $i; ?>"><?= $year - $i; ?></a>
                <?php if ($year - $i === $selectedYear) : ?>
                    <div class="list-group">
                        <?php foreach ($months as $monthNumber => $month) : ?>
                            <a href="dashboard.php?year=<?= $selectedYear; ?>&month=<?= $monthNumber; ?>" class="list-group-item <?= $monthNumber === $selectedMonth ? 'active' : ''; ?>"><?= $month; ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card mb-4">
            <div class="card-body">
                <strong style="font-size: 3em;"><?= $total; ?></strong><br>
                Visite<?= ($total > 1) ? 's' : ''; ?> total
            </div>
        </div>
        <?php if (isset($details)) : ?>
            <h2>Détails des visites pour le mois</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Jour</th>
                        <th>Nombre de visites</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($details as $detail) : ?>
                        <tr>
                            <td><?= $detail['day']; ?></td>
                            <td><?= $detail['total']; ?> visite<?= $detail['total'] > 1 ? 's' : ''; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
    </div>
<?php endif; ?>

</div>
</div>
</div>
<?php
$counterEnabled = false;
require 'elements/footer.php';
?>