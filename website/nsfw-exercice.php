<?php

/** not safe for work 
 * ===================
 * Système de limitation aux -18 ans ne peuvent accéder à la page
 * 
 */
require_once 'elements/functions.php';

$barrier = 18;
$cookieName = 'canenter';
$age = null;

$oldEnough = '';

if (isset($_COOKIE[$cookieName])) {
    $oldEnough = (bool) $_COOKIE[$cookieName];
}

if (!empty($_POST['age'])) {
    $age = (int) $_POST['age'];
    $oldEnough = (int) date('Y') - $age >= $barrier;
    setcookie($cookieName, (string) $oldEnough);
}

$list = array_combine(range(2012, 1919), range(2012, 1919));

require 'elements/header.php';

?>
<!--

- Demander à l'utilisateur sa date de naissance (champ select de 2012 à 1919)
- Persiter la date de naissance dans un cookie (cookie de session)
- Si l'utilisateur est assez agé, lui montrer le contenu
- Sinon on affiche un message d'erreur
-->

<?php if ($oldEnough) : ?>
    <div class="row">
        <div class="col-md-5">
            Contenu olé olé
        </div>
    </div>
<?php elseif ($oldEnough === false) : ?>
    <div class="row">
        <div class="alert alert-danger">
            Vous avez moins de 18 ans, vous ne pouvez accéder à ce site.
        </div>
    </div>
<?php else : ?>
    <form action="nsfw.php" method="post">
        <h1>Attention! Accès restreint</h1>
        <h2>Ce site est a accès restreint aux plus de 18 ans</h2>
        <p>Entrez votre date de naissance</p>
        <div class="form-group">
            <?= select('age', '', $list); ?>
        </div>
        <button class="btn btn-primary">Valider</button>
    </form>
<?php endif; ?>
<?php $removeNewsletterFooter = true;
require 'elements/footer.php'; ?>