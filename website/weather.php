<?php
// require_once __DIR__ . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'OpenWeather.php';
// require_once __DIR__ . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'CurlException.php';
// require_once __DIR__ . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'HTTPException.php';
// require_once __DIR__ . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'UnauthorizedHTTPException.php';

use App\Exceptions\CurlException;
use App\Exceptions\HTTPException;
use App\OpenWeather;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

$withApiKey = false;
$localFile = false;
$file = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'weather' . date('Y-m-d-H-i-s') . '.json';
$file = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'forecast.json';

$config = require __DIR__ . '/config/env.php';
$apiKey  = $config['apiKey'];
$city = 'Limoges, FR';
$unit = 'metric';
$lang = 'fr';

/* if ($localFile && file_exists($file)) {
    $data = file_get_contents($file);
} else {
    if ($withApiKey) {
        $apiKey = '4114c43aa654cd2217cc5fa6931e87d9';
        $city = 'Limoges, FR';
        $unit = 'metric';
        $lang = 'fr';
        // $url = "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID={$apiKey}";
        // $url = sprintf('http://api.openweathermap.org/data/2.5/forecast?id=%s&units=%s&APPID=%s&', $city, );
        $url = "http://api.openweathermap.org/data/2.5/forecast?q={$city}&APPID={$apiKey}&units={$metric}&lang={$lang}";
    } else {
        $url = 'https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22';
    }
} */

$id = 6451740;
$weather = new OpenWeather($apiKey);
$weather->file = $file;

$error = null;

try {
    $forecast = $weather->getForecast($city);
    $today = $weather->getToday($city);
} catch (CurlException $e) {
    exit($e->getMessage());
} catch (HTTPException $e) {
    $error = $e->getCode() . ' : ' . $e->getMessage();
} catch (\Exception $e) {
    $error = $e->getMessage();
}
// $forecast = $weather->getForecast(null, $id);

// header('Content-Type: application/json');
/* if ($localFile && file_exists($file)) {
    $data = file_get_contents($file);
} else {
    if ($withApiKey) {
        $apiKey = '4114c43aa654cd2217cc5fa6931e87d9';
        $city = 'Limoges, FR';
        $unit = 'metric;'
        $lang = 'fr';
        // $url = "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID={$apiKey}";
        // $url = sprintf('http://api.openweathermap.org/data/2.5/forecast?id=%s&units=%s&APPID=%s&', $city, );
        $url = "http://api.openweathermap.org/data/2.5/forecast?q={$city}&APPID={$apiKey}&units={$metric}&lang={$lang}";

    } else {
        $url = 'https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22';
    }
    $curl = curl_init($url);
    curl_setopt_array($curl, [
        CURLOPT_CAINFO =>  __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'openweathermap-org.pem',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 1
    ]);
    $data = curl_exec($curl);
    if ($data === false) {
        die();
        var_dump(curl_error($curl));
    } else {
        if ($localFile) {
            file_put_contents($file, $data);
        }
    }
    var_dump(curl_getinfo($curl, CURLINFO_CERTINFO), curl_getinfo($curl, CURLINFO_HTTP_CODE));
    if (curl_getinfo($curl, CURLINFO_HTTP_CODE) === 200) {
        $data = json_decode($data, true);
        $message  = 'Il fera '.$data['main']['temp'] . '°C';
        echo $message;
    }
    curl_close($curl);
    echo $data;
}
 */

require 'elements/header.php';
?>
<div class="container">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $error; ?>
        </div>
    <?php else : ?>
        <ul>
            <li>En ce moment: <?= $today['date']->format('d/m/Y H\h'); ?>: <?= $today['description'] ?> <?= $today['temp'] ?>°C
            </li>
            <?php foreach ($forecast as $day) : ?>
                <li class="list-list-group-item">
                    <!-- <?= $day['date']->format('d/m/Y H\h'); ?>: <?= $day['description'] ?> <?= $day['temp'] ?>°C -->
                    <strong><?= $day['date']->format('d/m/Y'); ?></strong> <?= $day['date']->format('H:i'); ?> <?= $day['description'] ?> <?= $day['temp'] ?>°C
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
<?php
$counterEnabled = false;
require 'elements/footer.php'; ?>