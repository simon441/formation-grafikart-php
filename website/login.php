<?php
require_once 'vendor/autoload.php';
// require_once 'elements/functions.php';

$title = 'Log in';

$success = null;
$error = null;
$username = null;
$hashed = '$2y$12$qR4bC/JcHE4VtKZh86gQt.hyzPyXY4RAIquBipl9d.3wLqvWRNacu';

if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // if ($) {
    if ($_POST['username'] === 'John' && password_verify($_POST['password'], $hashed)) {
        // on connecte l'utilisateur

        session_start();
        $_SESSION['loggedin'] = 1;
        header('location: /dashboard.php');
        exit();
    } else {
        $error = "Identifiants incorrects";
    }
}
// echo password_hash('Doe', PASSWORD_DEFAULT);
require_once 'functions/auth.php';

if (isLoggedin()) {
    header('Location: /dashboard.php');
    exit();
}

require_once 'elements/header.php';
?>
<h1>Se connecter</h1>

<?php if ($error) : ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php endif; ?>
<div class="">
    <form action="" method="POST">
        <div class="form-group">
            <input type="text" name="username" placeholder="Nom d'utilisateur" required class="form-control" value="" autocomplete="off">
        </div>
        <div class="form-group">
            <input type="password" name="password" placeholder="Votre mot de passe" required class="form-control" value="" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>

</div>

<?php
$counterEnabled = false;
$removeNewsletterFooter = true;
require 'elements/footer.php'; ?>