<?php
// require_once 'elements/functions.php';
require_once 'vendor/autoload.php';

$title = 'Newsletter';

$success = null;
$error = null;
$email = null;

if (!empty($_POST['email'])) {
    $email = $_POST['email'];
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'emails' . DIRECTORY_SEPARATOR . date('Y-m-d');
        file_put_contents($file, $email . PHP_EOL, FILE_APPEND);
        $success = " Vous email a bien été enregisté";
        $email = null;
    } else {
        $error = "Email invalide";
    }
}

require 'elements/header.php';
?>
<h1>S'inscrire à la newsletter</h1>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, nostrum. Corporis pariatur aut nostrum cumque libero quidem, dignissimos alias maiores quisquam eaque neque iste aliquam commodi, suscipit quasi natus repudiandae.</p>

<?php if ($success) : ?>
    <div class="alert alert-success">
        <?= $success; ?>
    </div>
<?php endif; ?>
<?php if ($error) : ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php endif; ?>
<div class="row">
    <form action="" method="POST" class="form-inline">
        <div class="form-group">
            <input type="email" name="email" placeholder="Entrer votre email" required class="form-control" value="<?= htmlentities($email); ?>">
        </div>
        <button type="submit" class="btn btn-primary">S'inscrire</button>
    </form>

</div>

<?php $removeNewsletterFooter = true;
require 'elements/footer.php'; ?>