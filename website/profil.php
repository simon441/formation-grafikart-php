<?php
// require_once 'elements/functions.php';
require_once 'vendor/autoload.php';

$name = null;
$surname = null;
$age = null;

if (!empty($_GET['action']) && $_GET['action'] === 'logout') {
    unset($_COOKIE['utilisateur']);
    setcookie('utilisateur', '', time() - 3600);
}

if (!empty($_COOKIE['utilisateur'])) {
    $data = unserialize($_COOKIE['utilisateur']);
    extract($data);
}

if (!empty($_POST['name']) && !empty($_POST['surname'] && !empty($_POST['age']))) {
    $data = serialize([
        'name' => $_POST['name'],
        'surname' => $_POST['surname'],
        'age' => $_POST['age']
    ]);
    setcookie('utilisateur', $data);
    $name = $_POST["name"];
}
$title = 'Profil';

if ($name) {
    $title .= ' de ' . htmlentities($name);
}

require 'elements/header.php';
?>

<?php if ($name) : ?>
    <h1>Bonjour <?= htmlentities($surname); ?> <?= htmlentities($name); ?>, <?= (int) $age; ?> ans</h1>
    <a href="profil.php?action=logout">Se déconnecter</a>
<?php else : ?>

    <form action="profil.php" method="post">
        <div class="form-group">
            <label for="username" class="sr-only">Entrer votre nom: </label>
            <input type="text" id="username" class="form-control" name="name" placeholder="Entrer votre nom">
        </div>
        <div class="form-group">
            <label for="usersurname" class="sr-only">Entrer votre prénom: </label>
            <input type="text" id="usersurname" class="form-control" name="surname" placeholder="Entrer votre prénom">
        </div>
        <div class="form-group">
            <label for="age" class="sr-only">Entrer votre nom: </label>
            <input type="text" id="age" class="form-control" name="age" placeholder="Entrer votre age">
        </div>
        <button type="submit" class="btn btn-primary">S'enregister</button>
    </form>
<?php endif; ?>
<?php
require 'elements/footer.php'; ?>