<?php
class Message
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $message;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var string[]
     */
    private $errors;


    /**
     * __construct
     *
     * @param  string $username
     * @param  string $message
     * @param  DateTime $date
     *
     * @return void
     */
    public function __construct(string $username, string $message, ?DateTime $date = null)
    {
        $this->username = $username;
        $this->message = $message;
        if ($date === null) {
            $date = new DateTime();
        }
        $this->date = $date;
    }

    /**
     * isValid
     *
     * @return bool
     */
    public function isValid(): bool
    {
        if (strlen($this->username) < 3) {
            $this->errors['username'] = "Ce pseudo est trop court";
        }
        if (strlen($this->message) < 10) {
            $this->errors['message'] = "Ce message est trop court";
        }
        // var_dump($this->errors, strlen($this->username), strlen($this->message), $this->errors === null);
        return $this->errors === null;
    }

    /**
     * getErrors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * toHTML
     *
     * @return string
     */
    public function toHTML(): string
    {
        $pseudo = $this->username;
        $date = $this->date->format('d/m/Y à H:i');
        $message = nl2br($this->message);
        return <<<HTML
        <p>
            <strong>$pseudo</strong> <em>le $date</em><br>
            $message
        </p>
HTML;
    }

    /**
     * toJSON
     *
     * @return string
     */
    public function toJSON(): string
    {
        // $message = new Message($this->username, $this->message, $this->date);
        $data = [
            'username' => $this->username,
            'message' => $this->message,
            'date' => $this->date,
        ];
        $json = json_encode($data);
        return $json;
    }

    /**
     * fromJSON
     *
     * @param  string $json
     *
     * @return Message
     */
    public static function fromJSON(string $json): Message
    {
        $data = json_decode($json, true);
        return new Message($data['username'], $data['message'], new DateTime($data['date']['date']));
    }
}
