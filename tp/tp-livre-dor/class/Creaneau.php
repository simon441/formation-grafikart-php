<?php

/**
 * Class Creneau
 * Manages a creneau horaire
 */
class Creaneau
{
    public $debut;

    public $fin;

    public function __construct(int $debut, int $fin)
    {
        $this->debut = $debut;
        $this->fin = $fin;
    }

    public function toHTML(): string
    {
        return "<strong>{$this->debut}h</strong> à <strong>{$this->fin}h</strong>";
    }

    /**
     * inclusHeure
     * is the hour in the interval between debut and fin
     *
     * @param  mixed $hour
     *
     * @return bool
     */
    public function inclusHeure(int $hour): bool
    {
        return $hour >= $this->debut && $hour < $this->fin;
    }

    /**
     * intersect
     * if creaneau début or créneau fin in $this or $this surrounds $creaneau ($this is inclued in $creneau)
     *
     * @param Creaneau $creaneau
     *
     * @return bool
     */
    public function intersect(Creaneau $creaneau): bool
    {
        return
            $this->inclusHeure($creaneau->debut) ||
            $this->inclusHeure($creaneau->fin)
            || ($this->debut > $creaneau->debut && $this->fin < $creaneau->fin);
    }
}
