<?php

class Counter
{
    const INCREMENT = 1;

    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * incrementCounter
     * Increment a counter for a counter file
     *
     * @param  mixed $filePath
     *
     * @return void
     */
    public function increment(): void
    {
        $counter = 1;
        // Vérifier si le compteur existe
        if (file_exists($this->path)) {
            $viewed = file_get_contents($this->path);
            if ($viewed !== false) {
                $counter = (int) $viewed;
                $counter += static::INCREMENT;
            }

            // Si le fichier existe on incrémente

        }
        // Sinon on crée le fichier avec la valeur 1
        file_put_contents($this->path, $counter);
    }

    /**
     * numberViewed
     * returns the number of pages viewed
     *
     * @return string
     */
    public function get(): int
    {

        if (!file_exists($this->path)) {
            return 0;
        }
        $content = file_get_contents($this->path);
        return $content !== false ? (int) $content : 0;
    }
}
