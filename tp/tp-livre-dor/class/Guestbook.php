<?php
class Guestbook
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * __construct
     *
     * @param  string $file
     *
     * @return void
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * addMessage
     *
     * @param  Message $message
     *
     * @return void
     */
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;
        file_put_contents($this->file, $message->toJSON() . PHP_EOL, FILE_APPEND);
    }

    /**
     * getMessages
     *
     * @return array
     */
    public function getMessages(): array
    {
        $messages = [];
        if (!file_exists($this->file)) {
            return [];
        }

        $json = file($this->file);
        foreach ($json as $message) {
            $message = trim($message);
            $messages[] = Message::fromJSON($message);
        }
        return $messages;
    }
}
