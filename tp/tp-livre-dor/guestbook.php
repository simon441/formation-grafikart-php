<?php

// require_once 'class/Guestbook.php';
// require_once 'class/Message.php';
$username = '';
$userMessage = '';
$fichier = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'messages';
$error = null;
$success = null;
$errors = [];
$guestbook = new Guestbook($fichier);

var_dump($_POST);
if (!empty($_POST['username']) && !empty($_POST['message'])) {
    $username = $_POST['username'];
    $userMessage = $_POST['message'];
    $date = new DateTime();
    $message = new Message($username, $userMessage);
    if ($message->isValid()) {
        $guestbook->addMessage($message);
        $success = 'Merci pour votre message';
        $userMessage = null;
        $username = null;
    } else {
        $errors = $message->getErrors();
    }
}
require 'elements/header.php';
$counterEnabled = false;
?>
<h1>Livre d'or</h1>
<?php if ($success) : ?>
    <div class="alert alert-success">
        <?= $success; ?>
    </div>
<?php endif; ?>
<?php if ($error) : ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php endif; ?>
<div class="">
    <form action="" method="POST">
        <div class="form-group <?= isset($errors['username']) ? 'has-error' : ''; ?>">
            <input type="text" name="username" placeholder="Nom d'utilisateur" required class="form-control" value="<?= htmlentities($username); ?>" aria-describedby="username_help"><!-- minlength="3"-->
            <?php if (isset($errors['username'])) : ?>
                <!-- <div class="form-text text-muted">
                    <?= $errors['username']; ?>
                </div> -->
                <p id="username_help" class="help-block" style="color:red"><?= $errors['username']; ?></p>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <textarea name="message" id="message" rows="10" placeholder="Entrer votre message" required class="form-control"><?= htmlentities($userMessage); ?></textarea>
            <!--minlength="10"-->
            <?php if (isset($errors['message'])) : ?>
                <div class="" style="color:red;">
                    <?= $errors['message']; ?>
                </div>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>

</div>
<h2>Vos messages :</h2>
<?php $messages = $guestbook->getMessages(); ?>
<?php foreach ($messages as $message) : ?>
    <?= $message->ToHTML(); ?>
<?php endforeach; ?>
<?php $removeNewsletterFooter = true;
require 'elements/footer.php'; ?>

<!-- https://www.grafikart.fr/tutoriels/dates-php-1124 : 20:01 -->
<!-- https://youtu.be/HgIlzi6QzSc?t=1201 -->
<?php require 'elements/footer.php'; ?>