## Livre d'or

- On aura une page avec un formulaire
  - Un champ pour le nom d'utilisateur (input:text)
  - Un champ message (textarea)
  - Un bouton
    (le formulaire devra être validé et on n'acceptera pas les pseudos de moins de 3 caractères ni les messages de moins de 10 caractères)
- On créera un fichier "messages" qui contiendra un message par ligne
  - Pour "sérialiser" les messages on utilisera les fonctions `PHP json_encode(tableau)` et `json_decode(tableau, true)`
- La page devra afficher tous les messages sous le formulaire formaté de la manière suivante

```HTML
<p>
    <strong>Pseudo</strong> <em>le 03/12/2009 à 12h00</em><br>
    Le message
</p>
```

(Les sauts de lignes devront être conservés nl2br)

## Restriction

- Utiliser une classe pour représenter un Message,

  - `new Message(string $username, string $message, DateTime $date = null)`
  - `isValid(): bool`
  - `getErrors(): array`
  - `toHTML(): string`
  - `toJSON(): string`
  - `Message::fromJSON(string): Message`

- Utiliser une classe pour représenter le livre d'or
  - `new Guestbook($fichier)`
  - `addMessage(Message $message)`
  - `getMessages(): array`
