<?php

function countViews(): void
{
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter';
    $count = getNbPagesViewed();
    $count++;
    $cnt = file_put_contents($file, $count);
}

function getNbPagesViewed(): int
{
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter';
    $counter = 0;
    if (file_exists($file)) {
        $counter = file_get_contents($file);
        if ($counter !== false) {
            $counter = (int) $counter;
        }
    }
    return $counter;
}

/**
 * addViewed
 * add a visit to a page to the counter files
 *
 * @return void
 */
function addViewed(): void
{
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter';
    $dailyFile = $file . '-' . date("Y-m-d");

    incrementCounter($file);
    incrementCounter($dailyFile);
}

/**
 * incrementCounter
 * Increment a counter for a counter file
 *
 * @param  mixed $filePath
 *
 * @return void
 */
function incrementCounter(string $filePath): void
{
    $counter = 1;
    // Vérifier si le compteur existe
    if (file_exists($filePath)) {
        $viewed = file_get_contents($filePath);
        if ($viewed !== false) {
            $counter = (int) $viewed;
            $counter++;
        }

        // Si le fichier existe on incrémente

    }
    // Sinon on crée le fichier avec la valeur 1
    file_put_contents($filePath, $counter);
}

/**
 * numberViewed
 * returns the number of pages viewed
 *
 * @return string
 */
function numberViewed(): string
{

    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter';
    $content = file_get_contents($file);
    return $content !== false ? $content : "";
}

function numberViewedPerMonth(int $year, int $month): int
{
    $month = str_pad($month, 2, '0', STR_PAD_LEFT);
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter-' . $year . '-' . $month . '-' . '*';
    $files = glob($file);
    $total = 0;
    foreach ($files as $file) {
        $view = file_get_contents($file);
        if ($view !== false) {
            $total += (int) $view;
        }
    }
    return $total;
}

function numberViewedPerMonthDetailed(int $year, int $month): array
{
    /*
    [
        [
            'year' => 2019,
            'month' => 01,
            'day' => 06
            'total' => 7
        ]
    ]
    */
    $month = str_pad($month, 2, '0', STR_PAD_LEFT);
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'counter-' . $year . '-' . $month . '-' . '*';
    $files = glob($file);
    $total = 0;
    $details = [];
    foreach ($files as $file) {
        $view = file_get_contents($file);
        if ($view !== false) {
            // $total += (int) $view;
            // $details['total'] = (int) $view;
            $parties  = explode("-", basename($file));
            // var_dump($day);
            // exit();
            $details[] = [
                'year' => $parties[1],
                'month' =>  $parties[2],
                'day' =>  $parties[3],
                'total' => $view
            ];
        }
    }
    return $details;
}
