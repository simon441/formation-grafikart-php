<?php
function isLoggedin(): bool
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    return !empty($_SESSION['loggedin']);
}


function forceUserLogIn(): void
{
    if (!isLoggedin()) {
        header('Location: /login.php');
        require_once 'functions/counter.php';
    }
}
