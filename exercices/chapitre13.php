<?php
define('CRENEAUX', [
    [8, 12],
    [14, 19]
]);

function creaneauxHtml(array $creneaux): string
{
    $parts = [];

    // Construire le tableau intermédiaire 
    // de Xh à Yh
    // Implode pour construire la phrase finale
    foreach ($creneaux as $key => $creneau) {
        $parts[] = "de {$creneau[0]}h à {$creneau[1]}h";
    }

    return implode(' et ', $parts);
}

var_dump(creaneauxHtml(CRENEAUX));
