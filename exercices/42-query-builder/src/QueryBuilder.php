<?php

namespace App;

class QueryBuilder
{
    private $fields;

    private $from = [];

    private $where;

    private $order;

    private $limit;

    private $offset;

    private $params;

    public function from(string $table, ?string $alias = null): QueryBuilder
    {
        $this->from[] = $table . ($alias ? ' ' . $alias : '');
        return $this;
    }

    public function orderBy(string $field, string $direction): QueryBuilder
    {
        $direction = strtoupper($direction);
        $direction = in_array($direction, ["ASC", "DESC"]) ?  " " . $direction : "";
        $this->order[] = $field . $direction;
        return $this;
    }

    public function limit(int $limit): QueryBuilder
    {
        $this->limit = (int) $limit;
        return $this;
    }

    public function offset(int $offset): QueryBuilder
    {

        $this->offset = (int) $offset;
        return $this;
    }

    public function page(int $page): QueryBuilder
    {
        $this->offset($this->limit * ($page - 1));
        return $this;
    }

    public function where(string $where): QueryBuilder
    {
        $this->where[] = $where;
        return $this;
    }

    public function select(...$fields): QueryBuilder
    {
        // if (is_array($fields)) {
        //     $fields = array_map(function ($field) {
        //         return implode(', ', $field);
        //     }, $fields);
        // }
        $f = [];
        foreach ($fields as $value) {
            if (is_array($value)) {
                $f[] = implode(', ', $value);
            } else {
                $f[] = $value;
            }
        }
        $this->fields[] = implode(', ', $f);
        return $this;
    }

    public function setParam(string $name, $value): QueryBuilder
    {
        $this->params[$name] = $value;
        return $this;
    }

    public function count(\PDO $pdo): int
    {
        $sql = "SELECT ";
        $sql .= ' COUNT(id) AS c';
        $sql .= " FROM " . implode(", ", $this->from);
        $sql .=  $this->where ? " WHERE " . implode(', ', $this->where) : '';
        $result = $this->prepareQuery($pdo, $sql);
        return  $result ? $result['c'] :  null;
    }

    public function toSQL(): string
    {
        $sql = "SELECT ";
        $sql .= $this->fields ? implode(', ', $this->fields) : '*';
        $sql .= " FROM " . implode(", ", $this->from);
        $sql .=  $this->where ? " WHERE " . implode(', ', $this->where) : '';
        $sql .=  $this->order ? " ORDER BY " . implode(', ', $this->order) : '';

        $sql .=  $this->limit ? " LIMIT " . $this->limit : '';
        $sql .=  $this->offset !== null ? " OFFSET " . $this->offset : '';
        return $sql;
    }



    public function fetch(\PDO $pdo, string $field)
    {
        $result = $this->prepareQuery($pdo, $this->toSQL());
        return isset($result[$field]) ? $result[$field] : null;
    }

    public function prepareQuery(\PDO $pdo, string $sql)
    {
        $statement = $pdo->prepare($sql);
        $statement->execute($this->params);
        return $statement->fetch();
    }
}
