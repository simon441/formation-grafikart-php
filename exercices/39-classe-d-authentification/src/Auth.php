<?php

namespace App;

class Auth
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function login(string $login, string $password)
    {
        $statement = $this->pdo->prepare('SELECT * FROM users WHERE username=:u');
        $statement->execute(['u' => $login]);
        $user = $statement->fetch();
        if ($user && password_verify($password, $user['password'])) {
        }
        return false;
    }

    public function user()
    {
        return $_SESSION['user'];
    }
}
