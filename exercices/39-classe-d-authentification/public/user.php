<?php
session_start();
if (empty($_SESSION['user'])) {
    header('Location: login.php');
    exit();
}
?>
Réservé à l'utilisateur
<a href="logout.php">Se déconnecter</a>