<?php
session_start();
$error = null;
if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $pdo = new PDO("sqlite:../data.sqlite", null, null, [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);
    $statement = $pdo->prepare('SELECT * FROM users WHERE username=:u');
    $statement->execute(['u' => $_POST['username']]);
    $user = $statement->fetch();
    if ($user) {
        if (password_verify($_POST['password'], $user['password'])) {
            $_SESSION['user'] = $user;
            var_dump($_SESSION);
            switch ($user['role']) {
                case 'admin':
                    $url = 'admin.php';
                    break;
                case 'user':
                    $url = 'user.php';
                    break;
                default:
                    $url = '';
            }
            if ($url !== '') {
                header('Location: ' . $url);
                exit();
            }
        }
    }
    $error = "Identifiants invalides";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Se Connecter</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body class="p-4">
    <h1>Login</h1>
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $error; ?>
        </div>
    <?php endif; ?>
    <form action="" method="post">
        <div class="form-group">
            <input name="username" type="text" class="form-control" autocomplete="off" required>
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" autocomplete="off" required>
        </div>
        <button class="btn btn-primary">Se connecter</button>
    </form>
</body>

</html>