<?php
session_start();
if (empty($_SESSION['user'])) {
    header('Location: index.php');
    exit();
}
unset($_SESSION['user']);
header('Location: index.php');
exit();
