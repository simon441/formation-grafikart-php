<?php

namespace App;

class NumberHelper
{
    /**
     * Format the number
     *
     * @param  float $number
     *
     * @return string
     */
    public static function price(float $number, string $sigle = '€'): string
    {
        return number_format($number, 0, '', ' ') . ' ' . $sigle;
    }
}
