<?php
// exo1

echo 'EXERCICE 1';

function repondre_oui_ou_non($phrase)
{
    while (true) {
        $reponse = readline($phrase . '(o /n) ');
        if ($reponse === 'o') {
            return true;
        } elseif ($reponse === 'n') {
            return false;
        }
    }
}

$resultat = repondre_oui_ou_non('Voulez-vous continuer?');
// Si l'utilisateur tape 'o' => true
// Si l'utilisateur tape 'n' => false
var_dump($resultat);

echo 'EXERCICE 2 ' . PHP_EOL;
/*
function demander_creneau($question = null)
{
    if ($question === null) {
        $question = 'Veuillez entrer un créneau : ';
    }
    $reponse = readline($question);
    $creaneaux = [];
    while (true) {
        $ouverture = (int) readline('Heure d\'ouverture: ');
        $fermeture = (int) readline('Heure du fermeture: ');
        if ($ouverture >= $fermeture) {
            echo "Le créneau ne peut être enregistré car l'heure d'ouverture est ($ouverture) est supérieure à l'heure de fermeture ($fermeture).";
        } else {
            $creaneaux[] = [$ouverture, $fermeture];
            $action = readline('Entrer un nouveau créneau ? (n pour quitter) ');
            if ($action === 'n') {
                break;
            }
        }
    }
    return $creaneaux;
}

$creaneau = demander_creneau();
$creaneau2 = demander_creneau('Veuillez entrer votre créneau : ');
var_dump($creaneau, $creaneau2);
*/
function demander_creneau($phrase = 'Veuillez entrer un créneau : ')
{
    echo $phrase . "\n";
    while (true) {
        $ouverture = (int) readline('Heure d\'ouverture: ');
        if ($ouverture >= 0 && $ouverture <= 23) {
            break;
        }
    }
    while (true) {
        $fermeture = (int) readline('Heure du fermeture: ');
        if ($fermeture >= 0 && $fermeture <= 23 && $fermeture > $ouverture) {
            break;
        }
    }
    return [$ouverture, $fermeture];
}

$creaneau = demander_creneau();
$creaneau2 = demander_creneau('Veuillez entrer votre créneau : ');
var_dump($creaneau, $creaneau2);

echo 'EXERCICE 3 ' . PHP_EOL;


function demander_creneaux()
{
    $creaneaux = [];
    while (true) {
        $creaneaux[] = demander_creneau();
        $reponse = repondre_oui_ou_non();
        if ($reponse === 'n') {
            break;
        }
    }
    return $creaneaux;
}
$creaneaux = demander_creneaux();
var_dump($creaneaux);
