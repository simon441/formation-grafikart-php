# Chapters

## Les bases

1. Présentation de PHP (06 min)
2. Installer PHP sur MacOS (05 min)
3. Installer PHP sur Windows (05 min)
4. Installer PHP sur Linux (04 min)
5. Les variables (16 min)
6. Les tableaux (14 min)
7. Les conditions (22 min)
8. Les boucles (34 min)
9. Les fonctions (40 min)
10. Les fonctions utilisateurs (32 min)
11. Require & Include (10 min)
12. PHP & HTML (39 min)
13. Traitement des formulaires (1h01)
14. Les dates (50 min)
15. Lecture de fichiers (35 min)
16. Écriture de fichiers (16 min)
17. Les Cookies (35 min)
18. La session (08 min)

## Travaux pratiques

19. TP : Compteur de vues (13 min)
20. TP : Dashboard (32 min)
21. TP : Système de connexion (19 min)
22. Chiffrer les mots de passe (09 min)

## L'objet

23. L'objet DateTime (17 min)
24. Les class (18 min)
25. Statique (07 min)
26. L'héritage (19 min)
27. TP : Livre d'or (45 min)
28. Utiliser une API avec cURL (41 min)
29. Les Exceptions (26 min)
30. PHPDoc (09 min)
31. PDO (41 min)
32. Les espaces de noms (11 min)
33. L'autoloader ( 21 min)
34. Utiliser des librairies tierces (18 min)
35. Les fonctions anonymes (21 min)
36. Le router (31 min)
37. ob_start (09 min)

## Exercices

38. Exercice : Tableau dynamique (51 min)
39. Exercice : Classe d'authentification (40 min)
40. Exercice : Tester son code (15 min)
41. Exercice : Pratiquons les tests (27 min)
42. Exercice : QueryBuilder (32 min)
43. Exercice : Classe Table (37 min)

## Travaux Pratique 2

44. Présentation du projet (03 min)
45. Création de la structure (21 min)
46. Le routeur (18 min)
47. Remplir la base de données (15 min)
48. Listing d'articles (24 min)
49. Pagination (16 min)
50. Simplifions la gestion de l'URL (20 min)
51. Page article (23 min)
52. Page catégorie (10 min)
53. Réorganisation de la pagination (29 min)
54. Affichage des catégories sur le listing (30 min)
55. Création de la classe Table (34 min)
56. Administration du site (23 min)
57. Edition d'un article (30 min)
58. Gestion des formulaires (24 min)
59. Valider les données (23 min)
60. Création d'un article (19 min)
61. Gestion des catégories (35 min)
62. Liaison article catégorie (31 min)
63. Authentification (32 min)
64. Mise en ligne (26 min)
65. Upload d'images _Réservé aux membres premiums_ (56 min)

## Conclusion

66. Que faire maintenant ? (07 min)
