<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'Creaneau.php';
//1ière façon de faire
/*$creaneau = new Creaneau();
$creaneau->debut = 9;
$creaneau->fin = 12;
*/
// 2eme
$creaneau = new Creaneau(9, 12);
$creaneau2 = new Creaneau(14, 16);
var_dump($creaneau, $creaneau2, $creaneau->inclusHeure(10), $creaneau2->inclusHeure(10), $creaneau->intersect($creaneau2));

echo $creaneau->toHTML();
