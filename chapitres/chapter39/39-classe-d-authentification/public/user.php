<?php
require_once '../vendor/autoload.php';

use App\App;

$user = App::getAuth()->requireRole('user', 'admin');

?>
Réservé à l'utilisateur