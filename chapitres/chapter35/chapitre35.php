<?php

use Symfony\Component\VarDumper\VarDumper;

const SEPARATOR = "\n-------------------\n";

require_once 'vendor/autoload.php';

$maFonction = function ($nom) {
    return 'Salut ' . $nom;
};

// outputs 'Salut Jean'
echo $maFonction('Jean');

echo SEPARATOR;

$eleves = [
    [
        'nom' => 'Anne',
        'age' => 18,
        'moyenne' => 15
    ],
    [
        'nom' => 'Marc',
        'age' => 21,
        'moyenne' => 13
    ],
    [
        'nom' => 'Jean',
        'age' => 20,
        'moyenne' => 18
    ],
];

/**
 * sort by desc age
 */
$sortDescAge = function ($eleve1, $eleve2) {
    return $eleve1['age'] - $eleve2['age'];
};

$sorte = $eleves;
usort($sorte, $sortDescAge);

VarDumper::dump($sorte);
// (Anne, Jean, Marc)

echo SEPARATOR;

/**
 * sort by asc age
 */
$sortAscAge = function ($eleve1, $eleve2) {
    return $eleve2['age'] - $eleve1['age'];
};

$sorte = $eleves;
usort($sorte, $sortAscAge);

VarDumper::dump($sorte);
// (Marc, Jean, Anne)

echo SEPARATOR;

/** 
 * sort by asc moyenne
 */
$sortAscMoyenne = function ($eleve1, $eleve2) {
    return $eleve1['moyenne'] - $eleve2['moyenne'];
};

$sorte = $eleves;
usort($sorte, $sortAscMoyenne);

VarDumper::dump($sorte);
// (Marc, Anne, Jean)

echo SEPARATOR;

/**
 * Pouvoir changer la clé avec une variable qui change en dehors de la Closure => utiliser 'use'
 */
$key = 'moyenne';
$sort = function ($eleve1, $eleve2) use ($key) {
    return $eleve1[$key] - $eleve2[$key];
};

$sorte = $eleves;
usort($sorte, $sort);

VarDumper::dump($sorte);

echo SEPARATOR . SEPARATOR;

/**
 * Use a function
 */
/**
 * sortByKey
 *
 * @param  string $key
 *
 * @return Closure
 */
function sortByKey($key)
{
    return  function ($eleve1, $eleve2) use ($key) {
        return $eleve1[$key] - $eleve2[$key];
    };
}
// sort by age
$sorte = $eleves;
usort($sorte, sortByKey('age'));
VarDumper::dump($sorte);

// sort by moyenne
$sorte = $eleves;
usort($sorte, sortByKey('age'));
VarDumper::dump($sorte);

echo SEPARATOR . SEPARATOR;

/**
 * Use a function which sorts
 */
/**
 * sortByKey
 *
 * @param  string $key
 *
 * @return Closure
 */
function sorterByKey(array $array, string $key): array
{
    usort($array,  function ($a, $b) use ($key) {
        return $a[$key] - $b[$key];
    });
    return $array;
}
// sort by age
$elevesSorted = sorterByKey($eleves, 'age');
dump($eleves, $elevesSorted);

echo SEPARATOR;

// sort by moyenne$
$elevesSorted = sorterByKey($eleves, 'moyenne');
dump($elevesSorted, $eleves);


/**
 * array_filter
 */
$eleves = [
    [
        'nom' => 'Anne',
        'age' => 18,
        'moyenne' => 15
    ],
    [
        'nom' => 'Marc',
        'age' => 21,
        'moyenne' => 13
    ],
    [
        'nom' => 'Jean',
        'age' => 20,
        'moyenne' => 9
    ],
];

// eleves qui ont plus de 10 de moyenne
$elevesMoyenne = array_filter($eleves, function ($eleve) {
    return $eleve['moyenne'] > 10;
});

dump($elevesMoyenne);

echo '>>>>>>>>>>>>>>>>>>>>' . SEPARATOR . '<<<<<<<<<<<<<<<<<<<<<<<' . PHP_EOL;
/**
 * Closure dans un objet
 */

class Demo
{
    private $eleves = [
        [
            'nom' => 'Anne',
            'age' => 18,
            'moyenne' => 15
        ],
        [
            'nom' => 'Marc',
            'age' => 21,
            'moyenne' => 13
        ],
        [
            'nom' => 'Jean',
            'age' => 20,
            'moyenne' => 9
        ],
    ];

    public function filterFunction($eleve)
    {
        return $eleve['moyenne'] > 10;;
    }

    public function bonEleves()
    {
        return $elevesMoyenne = array_filter($this->eleves, [$this, 'filterFunction']);
    }
}

$demo = new Demo();
dump($demo->bonEleves());
