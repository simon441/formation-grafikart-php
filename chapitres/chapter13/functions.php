<?php
// if (!function_exists('navItem')) {
/**
 * navItem
 * Returns a formatted link for a nav list, active link -> current page from its SCRIPT_NAME
 *
 * <li class="$className"><a class="$linkClass" href="$link">$title</a></li>
 * 
 * @param  mixed $link Page link (href)
 * @param  mixed $title Page title
 * @param  mixed $linkClass Class name of the li
 *
 * @return string
 */
function navItem(string $link, string $title, string $linkClass = ''): string
{
    $className = 'nav-item';
    $sr = '';
    if ($_SERVER['SCRIPT_NAME'] === $link) {
        $className .= " active";
        $sr = '<span class="sr-only">(page courante)</span>';
    }
    return <<<HTML
    <li class="$className">
        <a class="$linkClass" href="$link">$title</a>
    </li>
HTML;
}
// }

/**
 * navMenu
 * creates a full navigation menu
 *
 * @param  mixed $linkClass
 *
 * @return string
 */
function navMenu(string $linkClass = ''): string
{
    return navItem('/index.php', 'Accueil', $linkClass) .
        navItem('/contact.php', 'Contact', $linkClass);
}

/**
 * checkbox
 * creates a checkbox field
 *
 * @param  mixed $name The field name
 * @param  mixed $value The field VAlue
 * @param  mixed $data The input data
 *
 * @return string
 */
function checkbox(string $name, string $value, array $data): string
{
    $attributes = '';
    if (isset($data[$name]) && in_array($value, $data[$name])) {
        $attributes .= ' checked';
    }
    return <<<HTML
    <input type="checkbox" name="{$name}[]" value="$value" $attributes>
HTML;
}

/**
 * radio
 * creates a radio field
 *
 * @param  mixed $name The field name
 * @param  mixed $value The field Value
 * @param  mixed $data The input data
 *
 * @return string
 */
function radio(string $name, string $value, array $data): string
{
    $attributes = '';
    if (isset($data[$name]) && $value === $data[$name]) {
        $attributes .= ' checked';
    }
    return <<<HTML
    <input type="radio" name="{$name}" value="$value" $attributes>
HTML;
}

/**
 * dump
 * shows a variable in a nice format (from var_dump)
 *
 * @param  mixed $variable The variable to show
 *
 * @return void
 */
function dump($variable)
{
    echo '<pre>';
    var_dump($variable);
    echo '</pre>';
}

function dd($variable)
{
    dump($variable);
    die;
}
