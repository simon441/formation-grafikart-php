<?php
$aDeviner = 150;

$error = null;
$success = null;
$value = null;

if (isset($_POST['nombre'])) {
    $value =  (int) $_POST['nombre'];
    if ($value > $aDeviner) {
        $error = 'Votre nombre est trop grand';
    } elseif ($value < $aDeviner) {
        $error = 'Votre nombre est trop petit';
    } else {
        $success = "Bravo! Vous avez déviné le nombre <strong>$aDeviner</strong>.";
    }
}
require 'header.php';
?>

<?php if ($error) : ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php elseif ($success) : ?>
    <div class="alert alert-success">
        <?= $success; ?>
    </div>
<?php endif; ?>

<div class="form-group">
    <form action="/jeu-deviner.php" method="POST">
        <div class="form-group">
            <label for="nombre">Entrer un nombre</label>
            <input type="number" id="nombre" name="nombre" class="form-control" placeholder="entre 0 et 1000" value="<?= $value; ?>">
            <input type="text" name="demo" id="demo" class="form-control">
        </div>
        <button type="submit">Deviner</button>
    </form>
    <?php
    require 'footer.php';
