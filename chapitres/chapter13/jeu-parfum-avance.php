<?php
require_once 'functions.php';

// checkbox
$parfums = [
    'Fraise' => 4,
    'Chocolat' => 5,
    'Vanille' => 3,
];

// radio buttons
$cornets = [
    'Pot' => 2,
    'Cornet' => 3,
];

// Checkbox
$supplements = [
    'Pépite de chocolat' => 1,
    'Chantilly' => 0.5
];

$title = "Composer votre glace";

$ingredients = [];
$total = 0;
foreach (['parfum', 'supplement', 'cornet'] as $name) {
    if (isset($_GET[$name])) {
        $listePrix = $name . 's';
        $choix = $_GET[$name];
        if (is_array($choix)) {
            foreach ($choix as $value) {
                if (isset($$listePrix[$value])) {
                    $ingredients[] = $value;
                    $total += $$listePrix[$value];
                }
            }
        } else {
            if (isset($$listePrix[$choix])) {
                $ingredients[] = $choix;
                $total += $$listePrix[$choix];
            }
        }
    }
}

if (isset($_GET['cornet'])) {
    $cornet = $_GET['cornet'];
}



require 'header.php';
?>

<h1>Composer votre glace</h1>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Votre glace</h5>
                <ul>
                    <?php foreach ($ingredients as $ingredient) : ?>
                        <li><?= $ingredient; ?></li>
                    <?php endforeach; ?>
                </ul>
                <p>
                    <strong>Prix: </strong><?= $total; ?> €
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-8">

        <form action="/jeu-parfum.php" method="GET">
            <h2>Choississez vos parfums</h2>
            <?php foreach ($parfums as $parfum => $prix) : ?>
                <div class="checkbox">
                    <label>
                        <?= checkbox('parfum', $parfum, $_GET); ?>
                    </label>
                    <?= $parfum; ?> - <?= $prix; ?> €
                </div>
            <?php endforeach; ?>

            <h2>Choississez votre cornet</h2>
            <?php foreach ($cornets as $cornet => $prix) : ?>
                <div class="checkbox">
                    <label>
                        <?= radio('cornet', $cornet, $_GET); ?>
                    </label>
                    <?= $cornet; ?> - <?= $prix; ?> €
                </div>
            <?php endforeach; ?>

            <h2>Choississez vos suppléments</h2>
            <?php foreach ($supplements as $supplement => $prix) : ?>
                <div class="checkbox">
                    <label>
                        <?= checkbox('supplement', $supplement, $_GET); ?>
                    </label>
                    <?= $supplement; ?> - <?= $prix; ?> €
                </div>
            <?php endforeach; ?>


            <button type="submit" class="btn btn-outline-primary">Composer ma glace</button>
        </form>
    </div>
    <?php
    require 'footer.php';
