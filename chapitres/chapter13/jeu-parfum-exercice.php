<?php
// checkbox example
require 'header.php';

// checkbox
$parfums = [
    'Fraise' => 4,
    'Chocolat' => 5,
    'Vanille' => 3,
];

// radio buttons
$cornets = [
    'pot' => 2,
    'cornet' => 3,
];

// Checkbox
$supplements = [
    'Pépite de chocolat' => 1,
    'Chantilly' => 0.5
];

$parfum = $_GET['parfum'] ?? null;
$cornet = $_GET['cornet'] ?? null;
$supplement = $_GET['supplement'] ?? null;



/**
 * displayCheckbox
 *
 * @param  mixed $name Checkbox list names
 * @param  mixed $data Data to populate the fields
 * @param  mixed $selected Selected field
 *
 * @return string
 */
function displayCheckbox(string $name, array $data, $selected): string
{
    $html = '';
    foreach ($data as $key => $value) {
        // checked?
        $checked = $selected !== null && in_array($key, $selected) ? " checked" : '';
        $html .= <<<EOT
 <input type="checkbox" name="{$name}[]" $checked value="$key">$key<br>
 EOT;
    }
    return $html;
}
/**
 * displayCheckbox
 *
 * @param  mixed $name Checkbox list names
 * @param  mixed $data Data to populate the fields
 * @param  mixed $selected Selected field
 *
 * @return string
 */
function displayRadio(string $name, array $data, $selected = ''): string
{
    $html = '';
    foreach ($data as $key => $value) {
        // checked?
        $checked = $selected !== null && $key === $selected ? " checked" : '';
        var_dump(__METHOD__, __LINE__, $key, $value, $selected, $checked);
        $html .= <<<EOT
 <input type="radio" name="{$name}" $checked value="$key">$key<br>
 EOT;
    }
    return $html;
}


function calculeCornet(array $data, $selected): int
{
    $total = 0;
    if (!is_array($selected)) {
        $selected = [$selected];
    }
    foreach ($data as $key => $value) {
        if (in_array($key, $selected)) {
            $total += $value;
        }
    }
    return $total;
}


$total = 0;
if ($parfum !== null) {
    $total += calculeCornet($parfums, $parfum);
}
if ($cornet !== null) {
    $total += calculeCornet($cornets, $cornet);
}

if ($supplement !== null) {
    $total += calculeCornet($supplements, $supplement);
}
var_dump($total);
die;
?>

<pre>
    <?= var_dump($_GET, $parfum, $cornet, $supplement, '<hr>', $total); ?>
</pre>

<?php if ($total > 0) : ?>
    La glace vous coûtera <?= $total; ?> Euros.
<?php endif; ?>
<div class="form-group">
    <form action="/jeu-parfum-exercice.php" method="GET">
        <!-- <div class="form-group">
            <label for="parfum">Entrer un parfum</label><br>
            <input type="checkbox" name="parfum[]" value="Fraise">Fraise<br>
            <input type="checkbox" name="parfum[]" value="Vanille">Vanille<br>
            <input type="checkbox" name="parfum[]" value="Chocolat">Chocolat<br>
        </div> -->
        <div class="form-group">
            <label for="parfum">Entrer un parfum</label><br>
            <?= displayCheckbox('parfum', $parfums, $parfum); ?>
            <!-- <input type="checkbox" name="parfum[]" value="Fraise">Fraise<br>
            <input type="checkbox" name="parfum[]" value="Vanille">Vanille<br>
            <input type="checkbox" name="parfum[]" value="Chocolat">Chocolat<br> -->
        </div>
        <div class="form-group">
            <label for="">Entrer un type de cornet</label><br>
            <?= displayRadio('cornet', $cornets, $cornet); ?>

        </div>
        <div class="form-group">
            <label for="parfum">Entrer un supplément</label><br>
            <?= displayCheckbox('supplement', $supplements, $supplement); ?>
            <!-- <input type="checkbox" name="parfum[]" value="Fraise">Fraise<br>
            <input type="checkbox" name="parfum[]" value="Vanille">Vanille<br>
            <input type="checkbox" name="parfum[]" value="Chocolat">Chocolat<br> -->
        </div>
        <button type="submit">Choisir</button>
    </form>
    <?php
    require 'footer.php';
