<?php
/*
Exo1

Demander à l'utilisateur de taper une note ou taper "fin"
Chaque note est sauvegardée dans un tableau notes
à la fin on affiche le tableau de notes sous forme de liste
*/

$note = null;
$notes = [];

while ($note !== "fin") {
    $note = readline('Entrer une noter ou taper "fin" pour terminer: ');
    if ($note !== "fin") {
        $notes[] = (int) $note;
    }
}

foreach ($notes as $note) {
    echo "- $note\n";
}

/*
Exo2

Demander à l'utilisateur de rentrer les horaires d'ouverture d'un magasin
On demander à l'utilisateur de rentrer un heure et on lui dit si le magasin est ouvert
On affiche à l'utilisateur les horaires d'ouverture et de fermeture
*/

$horaires = [];

while (true) {
    $action1  = (int) readline('Entrer une horaire d\'ouverture: ');
    $action2  = (int) readline('Entrer une horaire de fermeture: ');

    if ($action1 < $action2) {
        $horaires[] = [$action1, $action2];
    } else {
        echo 'L\'heure de début ne peut être inférieure à l\'heure de fin';
    }

    $action = readline('Continuer(oui/non)? ');
    if ($action === "non") {
        break;
    }
}

$horaire = (int) readline('Entrer une horaire pour savoir si le magasin est ouvert: ');
$estOuvert = false;
foreach ($horaires as $value) {
    if (($value[0] <= $horaire && $value[1] <= $horaire)) {
        $estOuvert = true;
        break;
    }
}
if ($estOuvert) {
    echo 'Le magasin est ouvert.';
} else {
    echo 'Le magasin est fermé.';
}

echo PHP_EOL . PHP_EOL;

//????
echo "Horaires d'ouverture et de fermeture du magasin:\n";
foreach ($horaires as $horaire) {
    echo "- ${horaire[0]} à ${horaire[1]}\n";
}
