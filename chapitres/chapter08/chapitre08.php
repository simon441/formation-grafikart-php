<?php
// Boucles

$chiffre = null;
while ($chiffre !== 10) {
    $chiffre = (int) readline('Entrez une valeur: ');
}

echo 'Bravo vous avez gagné!';

echo PHP_EOL;

// afficher les 10 premier chiffres
for ($i = 1; $i <= 10; $i++) {
    echo "- $i \n";
}

// aller de 2 en 2
for ($i = 0; $i < 10; $i = $i + 2) {
    echo "- $i \n";
}

echo PHP_EOL;

// parcourir un tableau
$notes = [10, 15, 16];

for ($i = 0; $i < 3; $i++) {
    echo "- " .  $notes[$i] . "\n";
}

echo PHP_EOL;

foreach ($notes as $note) {
    echo "- $note \n";
}

echo PHP_EOL;

$eleves = [
    'cm2' => 'Jean',
    'cm1' => 'Marc',
];
foreach ($eleves as $classe => $eleve) {
    echo "$eleve est dans la classe $classe \n";
}


// Correction exo1

$notes = [];

$action = null;
while (true) {
    $action = readline('Entrer une noter ou taper "fin" pour terminer: ');
    if ($action === "fin") {
        break;
    }
    $notes[] = (int) $action;
}

foreach ($notes as $note) {
    echo "- $note\n";
}

// Correction exo2

// ALGORITHME
// On demande a l'utilisateur de rentrer un creaneau
//      On vérifie que l'heure de début < l'heure de fin
//      On demande si on veut rajouter un nouneau créneau (o/n)
// On demande à l'utilisateur de rentrer une heure
// On affiche l'état d'ouverture du magasion
$creaneaux = [];

while (true) {
    $ouverture = (int) readline('Heure d\'ouverture: ');
    $fermeture = (int) readline('Heure du fermeture: ');
    if ($ouverture >= $fermeture) {
        echo "Le créneau ne peut être enregistré car l'heure d'ouverture est ($ouverture) est supérieure à l'heure de fermeture ($fermeture).";
    } else {
        $creaneaux[] = [$ouverture, $fermeture];
        $action = readline('Entrer un nouveau créneau ? (n pour quitter) ');
        if ($action === 'n') {
            break;
        }
    }
}

$heure = (int) readline("A quelle heure voulez-vous visiter le magasin? ");

$creaneauTrouve = false;
foreach ($creaneaux as $creaneau) {
    if ($heure >= $creaneau[0] && $heure <= $creaneau[1]) {
        $creaneauTrouve = true;
        break;
    }
}

if ($creaneauTrouve) {
    echo 'Le magasin sera ouvert';
} else {
    echo 'Désolé, le magasin sera fermé :(';
}

// Le magasin est ouvert de 14 à 18h et de 8h à 12h
echo 'Le magasin est ouvert de';

foreach ($creaneaux as $k => $creaneau) {
    if ($k > 0) {
        echo ' et de ';
    }
    echo "{$creaneau[0]}h à {$creaneau[1]}h\n";
}
