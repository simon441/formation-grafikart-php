<?php
// les fonctions

// chercher un palidrome

$mot = readline("Veuillez entrer un mot: ");
$reversed = strtolower(strrev($mot));
if (strtolower($mot) === $reversed) {
    echo 'Ce mot est un palidrome';
} else {
    echo 'Ce mot n\'est pas un palidrome';
}

echo PHP_EOL;

// faire la moyenne d'une liste de notes

$notes = [10, 20, 13];

echo 'Vous avez ' . round(array_sum($notes) / count($notes), 2) . ' de moyenne.';

echo PHP_EOL;

// filtre à insultes
// récupérer une phrase puis remplacer les insultes par des * au même nombre de lettres que chaque mot remplacé
$insultes = ["merde", "con"];
$asterisques = [];


$phrase = readline("Veuillez entrer une phrase: ");

// methode 1
$phrase1 = $phrase;
foreach ($insultes as $insulte) {
    $replace = str_repeat('*', strlen($insulte));
    $phrase = str_ireplace($insulte, $replace, $phrase);
}

echo 'phrase filtrée: ' . $phrase;

echo "\n";

// methode 2
$phrase = $phrase1;
foreach ($insultes as $insulte) {
    $asterisques[] = str_repeat('*', strlen($insulte));
}
$phrase = str_ireplace($insultes, $asterisques, $phrase);

echo 'phrase filtrée: ' . $phrase;

echo PHP_EOL;

// filter les mot mais afficher la première lettre du mot
// exercice
$asterisques = [];
$phrase = $phrase1;
foreach ($insultes as $insulte) {
    // Trouver la première lettre du mot
    // Trouver la taille du mot -1
    // Concaténer la première lettre avec le résultat du str_repeat
    $lettre = $insulte[0];
    // $asterisques[] = $lettre . str_repeat('*', strlen($insulte) - 1);
    $asterisques[] = str_pad($lettre, strlen($insulte), '*');
}
$phrase = str_ireplace($insultes, $asterisques, $phrase);

echo 'phrase filtrée: ' . $phrase;

echo "\n";

// Grafikart
$asterisques = [];
$phrase = $phrase1;
foreach ($insultes as $insulte) {
    // Trouver la première lettre du mot
    // Trouver la taille du mot -1
    // Concaténer la première lettre avec le résultat du str_repeat
    $asterisques[] = substr($insulte, 0, 1) . str_repeat('*', strlen($insulte) - 1);
}
$phrase = str_ireplace($insultes, $asterisques, $phrase);

echo 'phrase filtrée: ' . $phrase;

echo PHP_EOL;

/*

EXERCICES COMPLEMENTAIRES SUGGERES

*/

// afficher [message supprimé] si contient au moins une insulte
$phrase = $phrase1;
$found = false;
foreach ($insultes as $insulte) {
    if (stripos($phrase, $insulte) !== false) {
        $found = true;
        break;
    }
}
if ($found) {
    echo '[message supprimé]';
} else {
    echo $phrase;
}

echo PHP_EOL;

// pas de phrase en majuscule
$phrase = $phrase1;
if (strtoupper($phrase) === $phrase) {
    echo "Pas de phrase entièrement en majuscules, c'est impoli.";
} else {
    echo 'La phrase est ok';
}
