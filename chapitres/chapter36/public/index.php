<?php
require_once '../vendor/autoload.php';

$uri = $_SERVER['REQUEST_URI'];

$router = new AltoRouter();



/* 
$router->map('GET', '/', function () {
    echo 'Hello from the homepage';
});

$router->map('GET', '/nous-contacter', function () {
    echo 'Nous contacter';
});
$router->map('GET', '/blog/[*:slug]-[i:id]', function ($slug, $id) {
    echo "Je suis sur l'article $slug avec le numéro $id";
});
 */
$router->map('GET', '/', 'home');
$router->map('GET', '/nous-contacter', 'contact', 'contact');
$router->map('GET', '/blog/[*:slug]-[i:id]', 'blog/article', 'blog.article.show');

$match = $router->match($uri);
// dump($match);

if (is_array($match)) {
    require_once '../elements/header.php';
    if (is_string($match['target'])) {
        require '../templates/' .  $match['target'] . '.php';
    } elseif (is_callable($match['target'])) {
        call_user_func_array($match['target'], $match['params']);
    } else {
        $match = false;
    }
    require_once '../elements/footer.php';
} else {
    $match = false;
}
if ($match === false) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    require_once '../templates/errors/404notfound.php';
}
