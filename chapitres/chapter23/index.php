<?php

/**
 * DateTime object
 */

$date = new DateTime();

/*object(DateTime)#1 (3) {
  ["date"]=>
  string(26) "2020-02-11 21:14:34.017903"
  ["timezone_type"]=>
  int(3)
  ["timezone"]=>
  string(3) "UTC"
}*/
var_dump($date);

echo "\n";

// outputs 11/02/2020
echo $date->format('d/m/Y');

echo "\n";

// modify date
$date = new DateTime();
$date->modify('+1 month');
// outputs 11/03/2020
echo $date->format('d/m/Y');

echo "\n";

/** date diff */

/* example 1 : quelques jours de différence */
$date1 = '2019-01-01';
$date2 = '2019-04-01';

$d1 = new DateTime($date1);
$d2 = new DateTime($date2);
// object(DateInterval)#4 (16) {
//   ["y"]=>
//   int(0)
//   ["m"]=>
//   int(3)
//   ["d"]=>
//   int(0)
//   ["h"]=>
//   int(0)
//   ["i"]=>
//   int(0)
//   ["s"]=>
//   int(0)
//   ["f"]=>
//   float(0)
//   ["weekday"]=>
//   int(0)
//   ["weekday_behavior"]=>
//   int(0)
//   ["first_last_day_of"]=>
//   int(0)
//   ["invert"]=>
//   int(0)
//   ["days"]=>
//   int(90)
//   ["special_type"]=>
//   int(0)
//   ["special_amount"]=>
//   int(0)
//   ["have_weekday_relative"]=>
//   int(0)
//   ["have_special_relative"]=>
//   int(0)
// }
var_dump($d1->diff($d2));

echo "\n";

$days = $d1->diff($d2)->days;

// IL y a 90 jours de différence
echo "IL y a {$days} jours de différence";

echo "\n\n";

/* example 2 : plusieures années de différence */
$date1 = '2014-01-01';
$date2 = '2019-04-01';

$d1 = new DateTime($date1);
$d2 = new DateTime($date2);
$diff = $d1->diff($d2);

// IL y a 90 jours de différence
echo "IL y a {$diff->y} années, {$diff->m} mois et {$diff->d} jours de différence";

/** DateInterval: intervalle de temps */
$interval = new DateInterval('P1M1DT1M');
var_dump($interval);
$date = new DateTime('2019-01-01');
$date->add($interval);
var_dump($date);
