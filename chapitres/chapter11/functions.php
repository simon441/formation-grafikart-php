<?php

/**
 * repondre_oui_ou_non
 *
 * @param  string|null $phrase
 *
 * @return bool
 */
function repondre_oui_ou_non(?string $phrase = null): bool
{
    while (true) {
        $reponse = readline($phrase . ' - (o)ui /(n)on : ');
        if ($reponse === 'o') {
            return true;
        } elseif ($reponse === 'n') {
            return false;
        }
    }
}

/**
 * demander_creneau
 *
 * @param  string $phrase
 *
 * @return array
 */
function demander_creneau(string $phrase = 'Veuillez entrer un créneau : '): array
{
    echo $phrase . "\n";
    while (true) {
        $ouverture = (int) readline('Heure d\'ouverture: ');
        if ($ouverture >= 0 && $ouverture <= 23) {
            break;
        }
    }
    while (true) {
        $fermeture = (int) readline('Heure du fermeture: ');
        if ($fermeture >= 0 && $fermeture <= 23 && $fermeture > $ouverture) {
            break;
        }
    }
    return [$ouverture, $fermeture];
}

/**
 * demander_creneaux
 *
 * @param  string $phrase
 *
 * @return array
 */
function demander_creneaux(string $phrase = 'Veuillez entrer vos créneaux : '): array
{
    $creaneaux = [];
    $continuer = true;
    while ($continuer) {
        $creaneaux[] = demander_creneau($phrase);
        $continuer = repondre_oui_ou_non('Voulez-vous continuer ? ');
    }
    return $creaneaux;
}
