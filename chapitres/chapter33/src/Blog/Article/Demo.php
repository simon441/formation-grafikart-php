<?php

namespace App\Blog\Article;

class Demo
{
    public function __construct()
    {
        echo 'Je suis la classe ' . __CLASS__ . ' dans le namespace ' . __NAMESPACE__;
    }
}
