<?php

use App\App;
use App\Auth;

require_once '../vendor/autoload.php';

session_start();
$pdo = new PDO("sqlite:../data.sqlite", null, null, [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

$auth = App::getAuth();
$error = false;

// if ($auth->user() !== null) {
//     header('Location: index.php');
//     exit();
// }

if (!empty($_POST)) {

    $user = $auth->login($_POST['username'], $_POST['password']);
    if ($user) {
        header('Location: index.php?login=1');
        exit();
    }
    $error = true;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Se Connecter</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body class="p-4">
    <h1>Se connecter</h1>
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            Identififant ou mot de passe incorrect
        </div>
    <?php endif; ?>
    <?php if ($_GET['forbid']) : ?>
        <div class="alert alert-danger">
            L'accès à la page est interdit
        </div>
    <?php endif; ?>
    <form action="" method="post">
        <div class="form-group">
            <input name="username" type="text" class="form-control" autocomplete="off" required placeholder="Pseudo">
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" autocomplete="off" required placeholder="Mot de passe">
        </div>
        <button class="btn btn-primary">Se connecter</button>
    </form>
    <?php dump($_SESSION); ?>
</body>

</html>