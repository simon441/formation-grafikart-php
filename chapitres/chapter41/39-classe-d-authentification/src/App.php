<?php

namespace App;

use PDO;

class App
{
    /**
     * @var PDO
     */
    private static $pdo;

    /**
     * @var Auth
     */
    private static $auth;


    /**
     * getPdo
     *
     * @return PDO
     */
    public static function getPdo(): PDO
    {
        if (!self::$pdo) {
            self::$pdo = new PDO("sqlite:../data.sqlite", null, null, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]);
        }
        return self::$pdo;
    }

    /**
     * getAuth
     *
     * @return Auth
     */
    public static function getAuth(): Auth
    {
        if (!self::$auth) {
            self::$auth = new Auth(self::getPdo(), '/login.php');
        }
        return self::$auth;
    }
}
