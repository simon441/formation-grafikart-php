<?php

namespace App;

use App\Exception\ForbiddenException;
use PDO;

class Auth
{
    private $pdo;

    private $loginPath;

    private $session;

    public function __construct(PDO $pdo, string $loginPath, array &$session)
    {
        $this->pdo = $pdo;
        $this->loginPath = $loginPath;
        $this->session = &$session;
    }

    /**
     * user
     *
     * @return User|null
     */
    public function user(): ?User
    {
        $id = $this->session['auth'] ?? null;
        if ($id === null) {
            return null;
        }
        $query = $this->pdo->prepare('SELECT * FROM users WHERE id = :id');
        $query->execute(['id' => $id]);
        $user = $query->fetchObject(User::class);
        return $user ?: null;
    }

    public function requireRole(string ...$roles): void
    {
        $user = $this->user();
        if ($user === null) {
            throw new ForbiddenException("Vous devez être connecté pour voir cette page ");

            // header("Location: {$this->loginPath}?forbid=1");
            // exit();
        }
        if (!in_array($user->role, $roles)) {
            $roles = implode(", ", $roles);
            $role = $user->role;
            throw new ForbiddenException("Vous n'avez pas le rôle suffisant \"$role\" (attendu: $roles)");
            // throw new ForbiddenException("Vous n'avez pas le rôle suffisant \"$role\" (attendu: $roles) pour accéder à la page");
        }
    }

    /**
     * login
     *
     * @param  mixed $username
     * @param  mixed $password
     *
     * @return User|null
     */
    public function login(string $username, string $password): ?User
    {
        // Find the corresponding user to the username
        $query = $this->pdo->prepare('SELECT * FROM users WHERE username = :username');
        $query->execute(['username' => $username]);
        $user = $query->fetchObject(User::class);
        if ($user === false) {
            return null;
        }
        // On vérifie grâce à la fonction password_verify que l'utilisateur correspond
        if (password_verify($password, $user->password)) {
            $this->session['auth'] = $user->id;
            return $user;
        }
        return null;
    }
}
