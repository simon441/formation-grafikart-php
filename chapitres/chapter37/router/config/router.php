<?php
/* 
$router->map('GET', '/', function () {
    echo 'Hello from the homepage';
});

$router->map('GET', '/nous-contacter', function () {
    echo 'Nous contacter';
});
$router->map('GET', '/blog/[*:slug]-[i:id]', function ($slug, $id) {
    echo "Je suis sur l'article $slug avec le numéro $id";
});
 */
$router->map('GET', '/', 'home');
$router->map('GET', '/nous-contacter', 'contact', 'contact');
$router->map('GET', '/blog/[*:slug]-[i:id]', 'blog/article', 'blog.article.show');
