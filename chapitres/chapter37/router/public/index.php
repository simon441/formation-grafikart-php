<?php
require_once '../vendor/autoload.php';

$uri = $_SERVER['REQUEST_URI'];

$router = new AltoRouter();

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config/router.php';

$match = $router->match($uri);
// dump($match);

if (is_array($match)) {
    if (is_string($match['target'])) {
        ob_start();
        require '../templates/' .  $match['target'] . '.php';
        $pageContent = ob_get_clean();
    } elseif (is_callable($match['target'])) {
        call_user_func_array($match['target'], $match['params']);
    } else {
        $match = false;
    }
    require_once '../elements/layout.php';
} else {
    $match = false;
}
if ($match === false) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    require_once '../templates/errors/404notfound.php';
}
