<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $pageTitle ?? 'Mon site'; ?></title>
    <meta name="description" content="<?= $pageDecription ?? '' ?>">
</head>

<body>
    <div class="container">
        <?= $pageContent; ?>
    </div>
    <?= $pageJavascripts ?? ''; ?>
</body>

</html>