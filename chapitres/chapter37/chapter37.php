<?php

const SEPARATOR = "\n-------------------\n";


/**
 * bufferisation (ob_*)
 */

// n'affiche rien 
echo '(ob_end_clean)' . PHP_EOL;
ob_start();
echo 'Salut';
ob_end_clean(); // fini la bufferisation et l'efface

echo SEPARATOR;

// affiche 
echo '(ob_end_flush)' . PHP_EOL;
ob_start();
echo 'Salut';
ob_end_flush(); // fini la bufferisation et l'affiche directement

echo SEPARATOR;

// n'affiche rien 
echo '(ob_get_clean)' . PHP_EOL;
ob_start();
echo 'Salut';
$data = ob_get_clean(); // fini la bufferisation et l'envoie dans une variable qyui peut le stocker
echo $data;

echo SEPARATOR;

unset($data);

// affiche 
echo '(ob_get_flush)' . PHP_EOL;
ob_start();
echo 'Salut';
$data = ob_get_flush(); // fini la bufferisation, l'affiche directement et l'envoie dans une variable
echo $data;

echo SEPARATOR;

unset($data);
