<?php
// if (!function_exists('navItem')) {
/**
 * navItem
 * Returns a formatted link for a nav list, active link -> current page from its SCRIPT_NAME
 *
 * <li class="$className"><a class="$linkClass" href="$link">$title</a></li>
 * 
 * @param  mixed $link Page link (href)
 * @param  mixed $title Page title
 * @param  mixed $linkClass Class name of the li
 *
 * @return string
 */
function navItem(string $link, string $title, string $linkClass = ''): string
{
    $className = 'nav-item';
    $sr = '';
    if ($_SERVER['SCRIPT_NAME'] === $link) {
        $className .= " active";
        $sr = '<span class="sr-only">(page courante)</span>';
    }
    return <<<HTML
    <li class="$className">
        <a class="$linkClass" href="$link">$title</a>
    </li>
HTML;
}
// }

function navMenu(string $linkClass = ''): string
{
    return navItem('/index.php', 'Accueil', $linkClass) .
        navItem('/contact.php', 'Contact', $linkClass);
}
