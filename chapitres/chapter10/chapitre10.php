<?php

// declare(strict_types=1);

// les fonctions utilisateurs

function bonjour($nom = null)
{
    if ($nom === null) {
        return "Bonjour\n";
    }
    return 'Bonjour ' . $nom . PHP_EOL;
}

// output Bonjour Jean
$salutation = bonjour('Jean');
echo $salutation;

// output Bonjour
$salutation = bonjour();
echo $salutation;

// CORRECTION EXO 1
function repondre_oui_ou_non(?string $phrase = null): bool
{
    while (true) {
        $reponse = readline($phrase . ' - (o)ui /(n)on : ');
        if ($reponse === 'o') {
            return true;
        } elseif ($reponse === 'n') {
            return false;
        }
    }
}

$resultat = repondre_oui_ou_non('Voulez-vous continuer?');
// Si l'utilisateur tape 'o' => true
// Si l'utilisateur tape 'n' => false
var_dump($resultat);

// CORRECTION EXO 2

function demander_creneau(string $phrase = 'Veuillez entrer un créneau : '): array
{
    echo $phrase . "\n";
    while (true) {
        $ouverture = (int) readline('Heure d\'ouverture: ');
        if ($ouverture >= 0 && $ouverture <= 23) {
            break;
        }
    }
    while (true) {
        $fermeture = (int) readline('Heure du fermeture: ');
        if ($fermeture >= 0 && $fermeture <= 23 && $fermeture > $ouverture) {
            break;
        }
    }
    return [$ouverture, $fermeture];
}

$creaneau = demander_creneau();
$creaneau2 = demander_creneau('Veuillez entrer votre créneau : ');
var_dump($creaneau, $creaneau2);

// CORRECTION EXO3

function demander_creneaux(string $phrase = 'Veuillez entrer vos créneaux : '): array
{
    $creaneaux = [];
    $continuer = true;
    while ($continuer) {
        $creaneaux[] = demander_creneau($phrase);
        $continuer = repondre_oui_ou_non('Voulez-vous continuer ? ');
    }
    return $creaneaux;
}
$creaneaux = demander_creneaux();
var_dump($creaneaux);

// Example de TyprError envoie d'un float, la fonction attend un string avec declare(strict_types=1): pas de conversion implicite
// demander_creneaux(1.2);
