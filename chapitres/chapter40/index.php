<?php

declare(strict_types=1);

use App\NumberHelper;
use App\TableHelper;
use App\UrlHelper;

define('PER_PAGE', 20);

require_once 'vendor/autoload.php';

$pdo = new PDO('sqlite:./products.db', null, null, [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION,
]);

$query = 'SELECT * FROM products';
$queryCount = 'SELECT COUNT(id) AS count FROM products';

$params = [];

// allowed fields to be sorted
$sortable = ['id', 'name', 'price', 'city', 'address'];

// Search by city
if (!empty($_GET['q'])) {
    $query .= " WHERE city LIKE :city";
    $queryCount .= " WHERE city LIKE :city";
    $params['city'] = '%' . $_GET['q'] . '%';
}

// Organisation
if ((!empty($_GET['dir'])) && in_array($_GET['sort'], $sortable)) {
    $direction = $_GET['dir'] ?? 'asc';
    if (!in_array($direction, ['asc', 'desc'])) {
        $direction = 'asc';
    }
    $query .= " ORDER BY " . $_GET['sort'] . " " . $direction;
}

// Pagination
$page = (int) ($_GET['p'] ?? 1);
// offset
$offset = ($page - 1) * PER_PAGE;


$query .= '  LIMIT '  . PER_PAGE . " OFFSET " . $offset;

$statement = $pdo->prepare($query);
$statement->execute($params);
// if ($statement->errorCode() !== 0000) {
// }
$products = $statement->fetchAll();

$statement = $pdo->prepare($queryCount);
$statement->execute($params);
$count = (int) $statement->fetch()['count'];

// $products = $pdo->query($query)->fetchAll();
$pages = ceil($count / PER_PAGE);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biens Immobiliers</title>
    <link rel="stylesheet" href="bootstrap.min.css">
</head>

<body class="p-4">
    <h1>Les biens immobiliers</h1>
    <form action="" class="mb-4">
        <div class="form-group">
            <input type="search" name="q" placeholder="Rechercher par ville" class="form-control" value="<?= htmlentities($_GET['q'] ?? ''); ?>">
        </div>
        <button class="btn btn-outline-primary">Rechercher</button>
    </form>
    <p class="text-md-center font-italic">Results found: <?= $count; ?></p>
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= TableHelper::sort('id', 'ID', $_GET); ?></th>
                <th><?= TableHelper::sort('name', 'Nom', $_GET); ?></th>
                <th><?= TableHelper::sort('price', 'Prix', $_GET); ?></th>
                <th><?= TableHelper::sort('city', 'Ville', $_GET); ?></th>
                <th><?= TableHelper::sort('address', 'Adresse', $_GET); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product) : ?>
                <tr>
                    <td>#<?= htmlentities($product['id']); ?></td>
                    <td><?= htmlentities($product['name']); ?></td>
                    <td><?= NumberHelper::price((float) $product['price']) ?></td>
                    <td><?= htmlentities($product['city']); ?></td>
                    <td><?= htmlentities($product['address']); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php if ($pages > 1 && $page > 1) : ?>
        <a href="?<?= UrlHelper::withParam($_GET, 'p', $page - 1); ?>" class="btn btn-primary">Page Précédente</a>
    <?php endif; ?>
    <?php if ($pages > 1 && $page < $pages) : ?>
        <a href="?<?= UrlHelper::withParam($_GET, 'p', $page + 1); ?>" class="btn btn-primary">Page Suivante</a>
    <?php endif; ?>

</body>

</html>