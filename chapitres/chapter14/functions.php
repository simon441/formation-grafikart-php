<?php
// if (!function_exists('navItem')) {
/**
 * navItem
 * Returns a formatted link for a nav list, active link -> current page from its SCRIPT_NAME
 *
 * <li class="$className"><a class="$linkClass" href="$link">$title</a></li>
 * 
 * @param  mixed $link Page link (href)
 * @param  mixed $title Page title
 * @param  mixed $linkClass Class name of the li
 *
 * @return string
 */
function navItem(string $link, string $title, string $linkClass = ''): string
{
    $className = 'nav-item';
    $sr = '';
    if ($_SERVER['SCRIPT_NAME'] === $link) {
        $className .= " active";
        $sr = '<span class="sr-only">(page courante)</span>';
    }
    return <<<HTML
    <li class="$className">
        <a class="$linkClass" href="$link">$title</a>
    </li>
HTML;
}
// }

/**
 * navMenu
 * creates a full navigation menu
 *
 * @param  mixed $linkClass
 *
 * @return string
 */
function navMenu(string $linkClass = ''): string
{
    return navItem('/index.php', 'Accueil', $linkClass) .
        navItem('/contact.php', 'Contact', $linkClass);
}

/**
 * checkbox
 * creates a checkbox field
 *
 * @param  mixed $name The field name
 * @param  mixed $value The field VAlue
 * @param  mixed $data The input data
 *
 * @return string
 */
function checkbox(string $name, string $value, array $data): string
{
    $attributes = '';
    if (isset($data[$name]) && in_array($value, $data[$name])) {
        $attributes .= ' checked';
    }
    return <<<HTML
    <input type="checkbox" name="{$name}[]" value="$value" $attributes>
HTML;
}

/**
 * radio
 * creates a radio field
 *
 * @param  mixed $name The field name
 * @param  mixed $value The field Value
 * @param  mixed $data The input data
 *
 * @return string
 */
function radio(string $name, string $value, array $data): string
{
    $attributes = '';
    if (isset($data[$name]) && $value === $data[$name]) {
        $attributes .= ' checked';
    }
    return <<<HTML
    <input type="radio" name="{$name}" value="$value" $attributes>
HTML;
}

/**
 * select
 * creates a select field
 *
 * @param  string $name The field name
 * @param  string $value The field value
 * @param  string $options The field <option>
 * @param  array|string $attributes The attributes to add to the field (class="...", etc...) as array or string
 *
 * @return string
 */
function select(string $name, string $value, array $options, $attributes = ''): string
{
    if (!is_array($attributes)) {
        $attributes = [$attributes];
    }
    $htmlOptions = [];
    foreach ($options as $key => $option) {
        $attributesOptions = $key === $value ? 'selected' : '';
        $htmlOptions[] = "<option value='$key' $attributesOptions>$option</option>";
    }
    return  "<select name='$name' " . implode(' ', $attributes) . ">" . implode($htmlOptions) . "</select>";
}

/**
 * dump
 * shows a variable in a nice format (from var_dump)
 *
 * @param  mixed $variable The variable to show
 *
 * @return void
 */
function dump(...$variable)
{
    echo '<pre>';
    var_dump(...$variable);
    echo '</pre>';
}

/**
 * dd
 * shows a variable in a nice format (from var_dump) and forcibly terminates the script
 *
 * @param  mixed $variable The variable to show
 *
 * @return void
 */
function dd(...$variable)
{
    dump(...$variable);
    die;
}

/**
 * creaneauxHtml
 *
 * @param  array $creneaux Creaneaux list to iterate over
 *
 * @return void
 */
function creaneauxHtml(array $creneaux)
{
    if (empty($creneaux)) {
        return 'Fermé';
    }
    // Construire le tableau intermédiaire 
    // de Xh à Yh
    // Implode pour construire la phrase finale
    $phrases = [];
    foreach ($creneaux as $creneau) {
        $phrases[] = "de <strong>{$creneau[0]}h</strong> à <strong>{$creneau[1]}h</strong>";
    }

    return 'Ouvert ' . implode(' et ', $phrases);
}

/**
 * inCreaneaux
 * Checks if the hour is in the creaneaux
 *
 * @param  int $heure Heure 
 * @param  int[] $creaneaux
 *
 * @return bool
 */
function inCreaneaux(int $heure, array $creaneaux): bool
{
    foreach ($creaneaux as $creneau) {
        $debut = $creneau[0];
        $fin = $creneau[1];
        if ($heure >= $debut && $heure < $fin) {
            return true;
        }
    }
    return false;
}
