<?php
$title = 'Nous contacter';

require_once 'config.php';
require_once 'functions.php';


/** chercher si le magasin est ouvert maintenant */
date_default_timezone_set('Europe/Paris');
// Récupérer l'heure d'aujourd'hui $heure
$heure = (int) date('G');
// Récupérer les creaneaux d'aujourd'hui $creneaux
$creneaux = CRENEAUX[(int) date('N') - 1];
// Récupérer l'état d'ouverture du magasin
// $ouvert = inCreaneaux($heure, [[0, 19]]);
$ouvert = inCreaneaux($heure, $creneaux);
$color = ''; //$ouvert ? 'green' : 'red';


/** chercher si le magasin est ouvert à l'heure demandée */
$day = null;
$hour = null;
$day = isset($_POST['day']) ? (int) $_POST["day"] : null;
$hour = isset($_POST['hour']) ? (int) $_POST["hour"] : 0;
$searchOpened = null;

if ($day !== null && isset(CRENEAUX[$day]) && $hour >= 0 && $hour <= 23) {
    $creneaux = CRENEAUX[$day];
    $searchOpened = inCreaneaux($hour, $creneaux);
    $classAlert = $searchOpened ? 'success' : 'danger';
    $textOpened = $searchOpened ? 'ouvert' : 'fermé';
}

require 'header.php';
?>
<div class="row">
    <div class="col-md 8">
        <h2>Nous contacter</h2>

        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tenetur explicabo odio facere optio amet? Modi ullam reiciendis incidunt aspernatur aliquid cum consectetur optio vitae, fuga nobis iure iste numquam facilis!</p>

    </div>
    <div class="col-md-4">
        <h2>Horaires d'ouverture</h2>
        <hr>
        <h4>Voir si le magasin est ouvert</h4>
        <?php if ($searchOpened !== null) : ?>
            <div class="alert alert-<?= $classAlert ?>">
                Le magasin sera <?= $textOpened; ?> <?= strtolower(JOURS[$day]); ?> à <?= $hour; ?>h
            </div>
        <?php endif; ?>
        <form action="" method="post">
            <div class="form-group">
                <label for="selectDay">Choisir le jour </label>
                <select name="day" id="selectDay" class="form-control">
                    <?php foreach (JOURS as $key => $jour) : ?>
                        <option value="<?= $key; ?>" <?= $key === $day ? ' selected' : ''; ?>><?= $jour; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="hour">Choisir l'heure </label>
                <input type="number" name="hour" id="hour" value="<?= $hour; ?>" min="0" max="23"> heures
            </div>
            <button type="submit" class="btn btn-outline-primary">Rechercher</button>
        </form>
        <hr>
        <?php if ($ouvert) : ?>
            <div class="alert alert-success">
                Le magasin est actuellement ouvert
            </div>
        <?php else : ?>
            <div class="alert alert-danger">
                Le magasin est actuellement fermé
            </div>
        <?php endif; ?>
        <!-- De 9h à 12h et de 14h à 19h -->
        <?php /* $creneaux */ ?>
        <ul>
            <?php foreach (JOURS as $key => $jour) : ?>
                <li <?php if ($key + 1 === (int) date('N')) : ?> style="color:<?= $color; ?>;" <?php endif; ?>> <strong><?= $jour; ?></strong> : <?= creaneauxHtml(CRENEAUX[$key]); ?></li>
            <?php endforeach; ?>
        </ul>

    </div>
</div>
<!-- https://www.grafikart.fr/tutoriels/dates-php-1124 : 20:01 -->
<!-- https://youtu.be/HgIlzi6QzSc?t=1201 -->
<?php require 'footer.php'; ?>