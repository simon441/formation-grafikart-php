<?php
$title = 'Nous contacter';
require_once 'functions.php';

$file = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'menu.tsv';

$handle = fopen($file, 'r');
$lines = [];


// }
// fclose($handle);

require 'header.php';
?>
<div class="">
    <?php
    $i = 0;
    while ($line = fgets($handle)) :
        $lineArray = explode("\t", $line);

        if (empty($lineArray[1])) :
            if ($i > 0) {
                echo '</ul>';
            }

            $i++;
    ?>
            <h2><?= $line; ?></h2>
            <ul>
            <?php
        else : ?>
                <li class="">
                    <strong><?= $lineArray[0]; ?></strong>
                    <br><?= $lineArray[1]; ?> €

                </li>
            <?php endif; ?>
        <?php
    endwhile;
    echo '</ul>';
    fclose($handle);
        ?>
</div>
<?php require 'footer.php'; ?>