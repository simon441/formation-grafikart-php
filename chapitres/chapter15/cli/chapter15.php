<?php

/**********
 * ECRITURE DANS UN FICHIER
 */
// chemin parent
// var_dump(dirname(__DIR__));

// chemin absolu vers un fichier
// $fichier = __DIR__ . DIRECTORY_SEPARATOR . 'demo.txt';

// écrire dans un fichier
// file_put_contents($fichier, 'salut les gens');

// Ajouter dans un fichier
// file_put_contents($fichier, 'marc comment ça va ?', FILE_APPEND);

// !!! Faire attention des permissions

// Traitement des erreurs (@ supprime l'affichage d'erreurs ou warnings)
/* $size = @file_put_contents($fichier, 'salut les gens');
if ($size === false) {
    echo "Impossible d'écrire dans le fichier";
} else {
    echo "Ecriture réussie";
} */


/**********
 * LECTURE D'UN FICHIER
 */

// Lire le contenu d'un fichier
// fichier local
// $fichier = __DIR__ . DIRECTORY_SEPARATOR . 'demo.txt';
// echo file_get_contents($fichier);
// fichier distant (pas le mieuxadapté pour les url distantes, à éviter)
// echo file_get_contents("https://jsonplaceholder.typicode.com/posts");


/**
 * CSV (Comma Separated Values)
 */
// fopen / fread/ fclose / fwrite

$fichier = __DIR__ . DIRECTORY_SEPARATOR . 'all.csv';

// LIRE UN FICHIER

$resource = fopen($fichier, 'r'); // ouvrir -> attacher un pointeur
// echo fread($resource, 2); // lire les 2 premiers octets
// var_dump(fstat($resource)); //  informations sur un fichier par un pointeur
// echo fgets($resource); // — Récupère la ligne courante sur laquelle se trouve le pointeur du fichier
// echo fgets($resource); // — Récupère la ligne courante sur laquelle se trouve le pointeur du fichier (ligne suivante pointeur++)

// itérer le contenu du fichier ligne par ligne pour atteindre la 123ième ligne
$k = 0; // itérateur
while ($line = fgets($resource)) {
    $k++;
    if ($k  === 123) {
        echo $line;
        break;
    }
}

fclose($resource); // fermer le handle (pointeur) sinon le fichier ne peut plus être manipulé


$fichier = __DIR__ . DIRECTORY_SEPARATOR . 'all.csv';

// ECRIRE DANS UN FICHIER

$resource = fopen($fichier, 'r+'); // ouvrir -> attacher un pointeur
// echo fread($resource, 2); // lire les 2 premiers octets
// var_dump(fstat($resource)); //  informations sur un fichier par un pointeur
// echo fgets($resource); // — Récupère la ligne courante sur laquelle se trouve le pointeur du fichier
// echo fgets($resource); // — Récupère la ligne courante sur laquelle se trouve le pointeur du fichier (ligne suivante pointeur++)

// écrite à la deuxième ligne du fichier : !!!attention!!! écrrase les données déjà présentes
$k = 0; // itérateur
while ($line = fgets($resource)) {
    $k++;
    if ($k  === 1) {
        fwrite($resource, 'salut les gens');
        break;
    }
}
fclose($resource);
