<?php

declare(strict_types=1);

use App\NumberHelper;
use App\QueryBuilder;
use App\Table;
use App\TableHelper;
use App\UrlHelper;

define('PER_PAGE', 20);

require_once '../vendor/autoload.php';

$pdo = new PDO('sqlite:../products.db', null, null, [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION,
]);

$query = (new QueryBuilder($pdo))->from('products');

// allowed fields to be sorted
$sortable = ['id', 'name', 'price', 'city', 'address'];

// Search by city
if (!empty($_GET['q'])) {
    $query
        ->where('city LIKE :city')
        ->setParam('city', '%' . $_GET['q'] . '%');
}

$table = (new Table($query, $_GET))
    ->sortable('id', 'city', 'price')
    ->format('price', function ($value) {
        return NumberHelper::price((float) $value);
    })
    ->columns([
        'id' => 'ID',
        'name' => 'Nom',
        'price' => 'Prix',
        'city' => 'Ville',
        'address' => 'Adresse',
    ]);

// $products = $pdo->query($query)->fetchAll();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biens Immobiliers</title>
    <link rel="stylesheet" href="bootstrap.min.css">
</head>

<body class="p-4">
    <h1>Les biens immobiliers</h1>
    <form action="" class="mb-4">
        <div class="form-group">
            <input type="search" name="q" placeholder="Rechercher par ville" class="form-control" value="<?= htmlentities($_GET['q'] ?? ''); ?>">
        </div>
        <button class="btn btn-outline-primary">Rechercher</button>
    </form>
    <?= $table->render(); ?>


</body>

</html>