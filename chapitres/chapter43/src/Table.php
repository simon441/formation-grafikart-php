<?php

namespace App;

class Table
{
    const SORT_KEY = 'sort';
    const DIRECTION_KEY = 'dir';

    /**
     * @var QueryBuilder
     */
    private $qb;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $columns = [];

    /**
     * @var array
     */
    private $sortable = [];

    private $formatters = [];


    /**
     * @var int
     */
    private $limit = 30;


    public function __construct(QueryBuilder $qb, array $data)
    {
        $this->qb = $qb;
        $this->data = $data;
    }

    public function sortable(string ...$sortable): self
    {
        $this->sortable = $sortable;
        return $this;
    }

    public function columns(array $columns): self
    {
        $this->columns = $columns;
        return $this;
    }

    public function format(string $key, callable $function): self
    {
        $this->formatters[$key] = $function;
        return $this;
    }

    private function th(string $key)
    {
        if (!(in_array($key, $this->sortable))) {
            return $this->columns[$key];
        }
        $sort = $this->data[self::SORT_KEY] ?? null;
        $direction = $this->data[self::DIRECTION_KEY] ?? null;

        $icon = "";
        if ($sort === $key) {
            $icon = $direction === 'asc' ? "^" : "v";
        }

        $url = UrlHelper::withParams($this->data, [
            self::SORT_KEY => $key,
            self::DIRECTION_KEY => $direction === 'asc' && $sort === $key ? 'desc' : 'asc'
        ]);
        return <<<HTML
        <a href="?$url">{$this->columns[$key]} $icon</a>        
HTML;
    }

    private function td(string $key, $item)
    {
        if (isset($this->formatters[$key])) {
            return $this->formatters[$key]($item[$key]);
        }
        return $item[$key];
    }

    public function render(): string
    {
        $page = $this->data['p'] ?? 1;
        $count = (clone $this->qb)->count();

        // Organisation
        if ((!empty($this->data['dir'])) && in_array($this->data['sort'], $this->sortable)) {
            $this->qb->orderBy($this->data['sort'], $this->data['dir'] ?? 'asc');
        }

        // pagination
        $items = $this->qb
            ->select(array_keys($this->columns))
            ->limit($this->limit)
            ->page($page)
            ->fetchAll();

        $pages = ceil($count / $this->limit);
        ob_start();

?>
        <p class="text-md-center font-italic">Results found: <?= $count; ?></p>
        <table class="table table-striped">
            <thead>
                <tr>
                    <?php foreach ($this->columns as $key => $column) : ?>
                        <th><?= $this->th($key); ?></th>
                        <?php /*<!-- <th><?= $this->th('id'); ?></th>
                        <th><?= $this->th('name'); ?></th>
                        <th><?= $this->th('price'); ?></th>
                        <th><?= $this->th('city'); ?></th>
                        <th><?= $this->th('address'); ?></th> -->*/ ?>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <?php foreach ($this->columns as $key => $column) : ?>
                            <td><?= $this->td($key, $item); ?></td>
                            <?php /*<td><?= htmlentities($item['name']); ?></td>
                        <td><?= NumberHelper::price((float) $item['price']) ?></td>
                        <td><?= htmlentities($item['city']); ?></td>
                        <td><?= htmlentities($item['address']); ?></td>*/ ?>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php if ($pages > 1 && $page > 1) : ?>
            <a href="?<?= UrlHelper::withParam($this->data, 'p', $page - 1); ?>" class="btn btn-primary">Page Précédente</a>
        <?php endif; ?>
        <?php if ($pages > 1 && $page < $pages) : ?>
            <a href="?<?= UrlHelper::withParam($this->data, 'p', $page + 1); ?>" class="btn btn-primary">Page Suivante</a>
        <?php endif; ?>
<?php
        return ob_get_clean();
    }
}
